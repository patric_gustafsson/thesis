\chapter{Controllability of the Model}
In this chapter we try to answer some questions regarding the controllability of our model. What we will be on the lookout for is some special \emph{target attractor} that we want to control the network into. This will be the main focus of this chapter.

The first topic we will cover is that of a transformation of a given SRG network into an equivalent Boolean network. This has the benefit of allowing us to apply existing tools and algorithms on the network. We will show that the transformation can be carried out efficiently. While it might not be the perfect solution for the problems we present in this chapter it is a good starting point nonetheless.

In the second section in this chapter we present a more efficient approach to finding target attractors based on inspection of the SRG. We will show that we can determine purely based on the SRG if some special \emph{target attractors} can exist. We will be spending most of the time on this topic since we need to develop some new theory and definitions to explain the concepts in-depth.

Lastly we want to look at some special controllability problems. Here we do not present any novel or new methods but simply show that some existing algorithms and ideas can be applied to our model as well. Both directly on the SRG and trough the BN transformation. Most of the things in this section are still open research problems so we cannot clearly state that any of the solutions given is the optimal one.  

\section{Target attractor detection}
\label{sec:TargetAttractorDetecion}

In this section we consider a problem that is related to the controllability. Namely given some set of nodes, called \emph{target nodes} and a \emph{target configuration} for those nodes, does the dynamical system of the SRG have an attractor were this target configuration always holds. Another way to state the same thing is: does there exist an attractor where the target values stays constant? If we can answer this question then the next question would be how to control the network to this attractor. It should be noted here that we are considering a more constrained version of controllability, rather than controlling the network to any given state we are requiring that the target configuration should belong to an attractor. This is a reasonable requirement since most states will be nonsense states that have no real biological meaning, while attractors usually represent some core function of the cell \cite{gates2016control}. We formulate the question as a decidability problem called \textbf{UnanimousAttractorDetection} as follows:

\begin{problem}
	{UnanimousAttractorDetection}
	{A target set \(T \subseteq V\), and a target configuration \(\alpha = \{1, -1\}^{|T|}\)}
	{Is there an attractor \(C\) such that \(C_{i}|_{T} = \boldsymbol{\alpha}\), for all \(1 \leq i \leq l\), where \(l\) is the period of \(C\)?}
	\label{definition:targetAttractorDetection}
\end{problem}

We will call attractors that satisfies \textbf{UnanimousAttractorDetection} \emph{target attractors}. 

The first question we should ask ourselves is: does such attractors even exist in our model? The case is certainly true for \(l = 1\), since we can choose \(\alpha = \{-1\}^{|V|}\) and this is always a singleton attractor in our model. The question is more interesting when \(l \geq 2\). Having attractors where some nodes stay constant for longer periods of time symbolizes that they have some sort of ``agreement" or unanimity between them. We show by example that there exists attractors with \(l \geq 2\) and \(\alpha \neq \{-1\}^{|V|}\). This shows that the problem is well-defined and that our search is not meaningless. 

\begin{example}
	Consider the graph in Figure \ref{fig:ExampleNetworkConstantAttractor}. Assume that we have the target set \(V = \{x_1\}\) and have \(\boldsymbol{\alpha} = \{1\}\). That is, we want to find an attractor where the node \(x_1\) remains active throughout. The network in Figure \ref{fig:ExampleNetworkConstantAttractor} has such an attractor. It consist of the following states:
	\begin{align*}
		C = & \{\{1, 0, -1, -1, 0, -1, -1\}, \{1,-1,-1,0,0,-1,-1\}, \\
			&   \{1,-1,0,0,-1,-1,-1\}, \{1,0,0,-1,-1,-1,-1\}\}
	\end{align*}
  	It can be easily seen that \(C\) is a target attractor since it satisfies the needed properties, namely that the target value stays constant trough the attractor. If we denote the attractors in \(C\) by \(C_i\) then we have:
	
	\begin{align*}
		C_1 |_{\{x_1\}} = 1 \\
		C_2 |_{\{x_1\}} = 1 \\
		C_3 |_{\{x_1\}} = 1 \\
		C_4 |_{\{x_1\}} = 1
	\end{align*}
	We can see that the target node, \(x_1\), remains constant throughout the attractor as it should.
	\label{example:ConstantAttractor}
\end{example} 

\begin{figure}
	\centering
	\begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}]
	
	% Nodes
	\node[main node] (A) {$x_1$};
	\node[main node] (B) [right = of A] {$x_2$};
	\node[main node] (C) [below = of B] {$x_3$};
	\node[main node] (E) [below = of A] {$x_5$};
	\node[main node] (F) [left = of A] {$x_6$};
	\node[main node] (G) [left = of F] {$x_7$};
	\node[main node] (D) [left = of E] {$x_4$};
	
	\path[every node/.style={font=\sffamily\small}]
	(A) edge [-Bar, color=inhibit, bend right] node [] {} (E)
	(A) edge [-Bar, color=inhibit] node [] {} (C)
	(A) edge [-Bar, color=inhibit] node [] {} (B)
	(A) edge [-Bar, color=inhibit] node [] {} (F)
	(A) edge [-Bar, color=inhibit] node [] {} (D)
	(A) edge [-Bar, color=inhibit] node [] {} (D)
	(F) edge [-Bar, color=inhibit] node [] {} (A)
	(F) edge [color=active, bend right] node [] {} (G)
	(G) edge [color=active, bend right] node [] {} (F)
	(D) edge [color=active, bend right] node [] {} (C)
	(D) edge [-Bar, color=inhibit] node [] {} (G)
	(E) edge [color=active, bend right] node [] {} (A)
	(E) edge [color=active] node [] {} (D)
	(C) edge [color=active] node [] {} (B)
	(B) edge [color=active] node [] {} (E);
	\end{tikzpicture}
	\caption{Network that used as an example to show a case where a constant attractor of period greater than one exists.}
	\label{fig:ExampleNetworkConstantAttractor}
\end{figure}

One obvious way to solve this is to simply create the whole STG and then use a technique such as depth-first-search to find the cycle within each of the strongly connected components. The drawback of this method is that generating the STG takes exponential time and memory, namely \(\Omega(3^n)\), so something more efficient is desired. We can achieve a small improvement by keeping the nodes in the target set constant. This makes the number of possible states we need to check somewhat smaller. 

\begin{prop}
	\textbf{UnanimousAttractorDetection} can be solved in \(\mathcal{O}(3^{n - |\alpha|})\). 
\end{prop}

\begin{proof}
	Simply keep each node in the target set constant when generating the STG. This reduces the number of possible combinations to \(3^{n - |\alpha|}\).
\end{proof}

Our first try at a solution is to reduce the SRG network into a BN which we do in the next section.

\input{BNTransform.tex}

\input{TargetAttractorsFinding.tex}

\section{Controllability to target attractors}
In this section we will take a look at some controllability problems in the context of the SRG model. We think of the problems in the context of \emph{strong controllability}. When we mention strong controllability here we are talking about a special kind of controllability problems that we will define. The word strong again comes from the fact that we want the values of the nodes in the SRG to be unanimous, which was the whole ida behind the definitions of our model. We state two problems that we think are of importance but leave their solutions as open research problems. We give some possible solutions but do not claim that they are optimal. 

The first problem we want to consider is the following:

\begin{problem}
	{AnyStateAttractorControl}
	{An SRG(\(V, E\)) and a target attractor \(A\).}
	{A minimal set of driver nodes that allows the network to be controlled into the attractor from any state \(\boldsymbol{x}\) in one control.}
\end{problem}

The target attractor should be one that has the properties we presented in Section \ref{sec:TargetAttractorDetecion}. Control here is applied only at \(t = 1\), this is perhaps the most reasonable assumption, since control at more time instances are biologically difficult to achieve \cite{hou2019number}. Formally we define the concept of control as:

\begin{definition} (Control)
	Given a set of driver nodes \(d_i \in D\) and an assignment \(\mu : D \rightarrow \{1, -1\}\), a \emph{control} of a state \(\boldsymbol{x} = (x_1, \ldots, x_k)\) is defined as:
	
	\begin{equation}
		\mu \circ \boldsymbol{x} = (u_1, u_2, \ldots, u_k)
	\end{equation}
	
	where \(u_i\) is the application of the control values, namely: 
	
	\begin{equation}
		u_i = \begin{cases}
					x_i & \text{if } d_i \notin D \\
					\mu_i & \text{if } d_i \in D.
				\end{cases}
	\end{equation}
	Here each \(\mu_i\) can be any of the values \(\{1, -1\}\) that achieves the required control. 
\end{definition}

We will now discuss a possible solution to \textbf{AnyStateAttractorControl}. This problem can be at least partially solved by using a concept from graph theory called the feedback vertex set (FVS). The FVS is defined as a set of nodes whose removal makes the graph acyclic. The problem of finding such a set can be stated as follows:

\begin{problem}
	{FVS-Problem}
	{A directed graph \(G(V, E)\).}
	{A set of nodes \(V' \subseteq V\), such that removal of the nodes \(V'\) from \(G\) makes \(G\) acyclic.}
	\label{prob:FVS}
\end{problem}

Acyclic means that the graph contains no cycle. In \cite{mochizuki2013dynamics} they showed that the FVS can be used as driver nodes in a BN if the target states are restricted to attractors. In general we are interested in finding the minimal amount of driver nodes, for this reason we modify the definition of Problem \ref{prob:FVS} as:

\begin{problem}
	{Minimal FVS}
	{A directed graph \(G(V, E)\).}
	{A minimal set of nodes \(V' \subseteq V\), such that removal of the nodes \(V'\) from \(G\) makes \(G\) acyclic.}
	\label{problem:MinimalFVS}
\end{problem}

The inclusion of minimality is important since otherwise we could choose \(V' = V\) which would work but hardly be of any use. It is known that the problem of finding a minimal FVS is \(\mathcal{NP}\)-complete \cite{karp1972reducibility}. We now show an example of how we can use the FVS to locate a driver set that solves \textbf{AnyStateAttractorControl}. It is currently unknown if this approach works in general on any SRG.

\begin{example}
	Consider the SRG in Figure \ref{fig:ExampleNetworkConstantAttractor}. Assume that our target attractor is the attractor we used in Example \ref{example:ConstantAttractor}. A minimum FVS for the SRG is \(\{x_7, x_6, x_5, x_3\}\). The following control will guide the network from any state into the attractor:
	
	\begin{equation}
		\mu(x_7) = -1, \mu(x_6) = 1, \mu(x_5) = 1, \mu(x_3) = 1
	\end{equation}
	To see that this works consider what other values the non-driver nodes can have. The node \(x_1\) will become active no matter what since \(x_5\) is activating it. Node \(x_2\) will become inactive no matter what since \(x_1\) will be inactivating it in the next state. The last node, \(x_4\) will also become inactive because of \(x_1\). As we can see the network is now in a configuration that belongs to the attractor no matter what the staring state of the non-driver nodes are.  
\end{example}

The other problem we want to open up is called \textbf{AnystateConstantAttractorControl}. The problem is the same, we have some target attractor and want to find a control policy that allows us to go from other states into the attractor. What is different is now control is not only applied at \(t = 1\) but instead the nodes are kept constant at some given value. What we would like to happen is that all attractors ``collapse" into our target attractor \(A\). We formally define the problem as:

\begin{problem}
	{AnyStateConstantAttractorControl}
	{An SRG\((V, E)\) and a target attractor \(A\).}
	{A minimal set of driver nodes that allows the network to be controlled into the attractor from any state \(\boldsymbol{v}\) by keeping the driver nodes constant}
\end{problem}

Since the nodes are kept constant some states will now no longer be reachable. They will collapse into other states. Our target attractor will probably also look different but the important thing is that the target nodes in the target attractor have the correct values. This variant of controllability has to the authors best knowledge not been studied in any other kind of modeling framework before. We leave this as an open research problem.


