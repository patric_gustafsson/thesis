\section{Conditions for existence of target attractors}
The BN transform from the previous section turned out to be a suboptimal solution for finding target attractors. We will in this section go trough a more efficient approach that is based entirely on the SRG model.

We will derive necessary and sufficient conditions for when a target attractor can exist, with specific target values, in the SRG. We will give a proof that states if the graph is built in a certain way then there cannot exist a target attractor with some given target values \(\boldsymbol{\alpha}\). We will show that if one such attractor can exist then there may be multiple of such attractors. We will find that the conditions are necessary and sufficient for finding singleton target attractors. We also show that the conditions are necessary but not sufficient for periodic target attractors to exist. When we say \emph{target attractor} in this section we mainly mean singleton target attractor, unless otherwise stated.

The proof will also give the necessary values for such an attractor, allowing us to construct one of them. We start by stating our goal for this section:

\begin{restatable}[]{thm}{UnanimousAttractorDetection}
	\textbf{UnanimousAttractorDetection} \(\in \mathcal{P}\). 
	\label{thm:UnanimousAttractorDetection}
\end{restatable}

We first consider a more constrained version of \textbf{UnanimousAttractorDetection}, where we only have one target node and the target is that the node should be active inside the attractor. We start with this concept and then move on to the inactive case. Finally we show how to combine them and the end result is an algorithm that solve \textbf{UnanimousAttractorDetection}.

\subsection{Attractor with active target value}

As mentioned we start with a more constrained version of our problem. Here we only consider one node that should be active in the attractor. This problem can be formally formulated as:

\begin{problem}
	{UnanimousAttractorSingle}
	{An SRG\((V, E)\) and a target value \(\alpha = 1\) and a target node \(v\).}
	{``Yes" if there exist a target attractor \(A\) with \(v = 1\). ``No" otherwise.}
\end{problem}

We will show that this problem can be decided by inspecting the graph. We first note that if a node is to remain active in a target attractor then all nodes in \(\rho^{-}(v)\) must be inactive, otherwise \(v\) would become inactive in the next state. This in turns give a condition for the adjacent activation nodes for \(\rho^{-}(v)\), they must also be inactive, and their activation nodes must also be inactive. Here is a pattern. Each node that we enumerate in this way must stay inactive. We can safely ignore all incoming activation edges since they have no impact on whether \(v\) stays active or not.

We will now introduce some notation so we can discuss these things more easily. 

\begin{definition} (Positive path)
	A positive path is a directed path in the SRG that only follows edges labeled with a \(+\). 
\end{definition}

\begin{definition} (Negative path)
		A negative path is a directed path in the SRG that only follows edges labeled with a \(-\).
\end{definition}

With these two definitions we can now define the concept of \emph{inactive ancestors}. Intuitively a inactive ancestor is a node that must remain inactive for \(v\) to remain active. The set of inactive ancestors is then all nodes that must remain inactive for \(v\) to stay active. Formally we can define the set of inactive ancestors for activity with the help of positive paths:

\begin{definition} (Inactive ancestors for activity)
	The set of inactive ancestors for activity of a node \(v\) is defined as:
	\begin{equation*}
		\lambda^{-}(v) = \rho^{-}(v) \cup \{u : \text{there is a positive path from \(u\) to \(w\) where \(w \in \rho^{-}(v)\)}\}
	\end{equation*}
\end{definition}

\begin{figure}
	\centering	
	\begin{tikzpicture}
	
	\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
	\tikzset{edge/.style = {->,> = latex'}}
	
	% vertices
	\node[vertex] (v) at  (2.5,3) {$v = 1$};
	\node[vertex] (u1) at  (0, 0) {$u_1$};
	\node[vertex] (u2) at (1.5,0) {$u_2$};
	\node[vertex] (uk) at  (4.5,0) {$u_k$};
	
	% the positive path nodes
	\node[vertex] (w1) at (0, -2) {$w_1$};
	\node[vertex] (w2) at (1.5, -2) {$w_2$};
	\node[vertex] (wk) at (4.5, -2) {$w_l$};
	
	%positive path last nodes
	\node[vertex] (z1) at (0, -8) {$z_1$};
	\node[vertex] (z2) at (1.5, -8) {$z_2$};
	\node[vertex] (zk) at (4.5, -8) {$z_m$};
	
	%edges
	\draw[edge, -Bar, red] (u1) to (v);
	\draw[edge, -Bar, red] (u2) to (v);
	\draw[edge, -Bar, red] (uk) to (v);
	\path (u2) to node {\dots} (uk);
	
	% positive path layer 1 edges
	\draw[edge, green] (w1) to (u1);
	\draw[edge, green] (w1) to (u2);
	\draw[edge, green] (w1) to (uk);
	\draw[edge, green] (w2) to (u1);
	\draw[edge, green] (w2) to (u2);
	\draw[edge, green] (w2) to (uk);
	\draw[edge, green] (wk) to (u1);
	\draw[edge, green] (wk) to (u2);
	\draw[edge, green] (wk) to (uk);
	
	\path (w2) to node {\dots} (wk);
	
	% Vertical dot nodes and edges
	\path (w1) to node {\vdots} (z1);
	\path (w2) to node {\vdots} (z2);
	\path (wk) to node {\vdots} (zk);
	
	\node [shape=circle,minimum size=1.5em] (c1) at (0,-4) {};
	\node [shape=circle,minimum size=1.5em] (c2) at (1.5,-4) {};
	\node [shape=circle,minimum size=1.5em] (ck) at (4.5,-4) {};
	
	% positive path intermediate edges
	\draw[edge, green] (c1) to (w1);
	\draw[edge, green] (c1) to (w2);
	\draw[edge, green] (c1) to (wk);
	\draw[edge, green] (c2) to (w1);
	\draw[edge, green] (c2) to (w2);
	\draw[edge, green] (c2) to (wk);
	\draw[edge, green] (ck) to (w1);
	\draw[edge, green] (ck) to (w2);
	\draw[edge, green] (ck) to (wk);
	
	\node [shape=circle,minimum size=1.5em] (d1) at (0,-6) {};
	\node [shape=circle,minimum size=1.5em] (d2) at (1.5,-6) {};
	\node [shape=circle,minimum size=1.5em] (dk) at (4.5,-6) {};
	
	% positive path intermediate edges
	\draw[edge, green] (z1) to (d1);
	\draw[edge, green] (z1) to (d2);
	\draw[edge, green] (z1) to (dk);
	\draw[edge, green] (z2) to (d1);
	\draw[edge, green] (z2) to (d2);
	\draw[edge, green] (z2) to (dk);
	\draw[edge, green] (zk) to (d1);
	\draw[edge, green] (zk) to (d2);
	\draw[edge, green] (zk) to (dk);
	
	% The brace
	\draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=4ex}]
	(-0.5,-8) -- (5,-8) node[midway,yshift=-3em]{$\lambda^{-}(v)$};
	
	\end{tikzpicture}
	\caption{Illustration of a node with its inactive ancestors for activity, \(\lambda^{+}(v)\). The first layer of the graph is just \(v\)'s inhibitors. The second layer is all nodes that have a positive path of length one to any of the inhibitors. The \(k\):th layer is then all nodes that have a positive path of length \(k\) to any inhibition node of \(v\). Here we show the most general version of the figure when every node is connected to every node that has a positive path of \(k-1\). In general only one path is needed.}
	\label{figure:GraphNoAttractorProof1}
\end{figure}

The set of inactive ancestors can be seen graphically in Figure \ref{figure:GraphNoAttractorProof1}. There we have put the nodes in layers depending on the length of the positive path from each node. In general there can be more than one path and in that case we simply say that the graph shows the \emph{shortest} of those paths.

We can now state our first condition for \(v\) to remain active. In all the proposition and proofs we will assume unless otherwise stated, that \(v\) is active. 

\begin{prop}
	Given an SRG(\(V, E\)) network and a target value of \(\alpha = 1\) for some node \(v \in V\), all nodes belonging to \(\lambda^{-}(v)\) must be inactive for there to be a target attractor with \(v = 1\). 
	\label{prop:InactiveAncestorsProp}
\end{prop}

\begin{proof}
	Assume that a node \(u \in \lambda^{-}(v)\) is not inactive. If \(u \in \rho^{-}(v)\) then \(v\) will become non-active in the next state and no target attractor with \(v = 1\) can exist. If \(u \notin \rho^{-}(v)\), then there must be a positive path from \(u\) leading to a node \(w \in \rho^{-}(v)\). Assume the length of this path is \(k\). Then \(w\) will become either 0 or 1 in \(k\) timesteps and in turn make \(v\) non-active in the \(k + 1\):th state. We have now showed that if any node in \(\lambda^{-}(v)\) is active then no target attractor with \(v = 1\) can exist.
\end{proof}

We are now ready to also consider the case when \(v\) has outgoing edges. We know already that the only nodes that are important for \(v\) to stay active is exactly the set \(\lambda^{-}(v)\). Thus only edges that go into that set will have any impact on \(v\)'s dynamics. It can quite easily be seen that a inhibition edge into the set of inactive ancestors will make no difference, since by assumption they must all be inactive anyway. This leaves the activation edge. The relationship between the activation edge and the inactive ancestors is the following:

\begin{prop}
	If the target node \(v\) with \(\alpha = 1\) has at least one activation edge to any node in \(\lambda^{-}(v)\) then no target attractor is possible with \(v = 1\).
	\label{prop:InactiveAncestorsPropEdge}
\end{prop}

\begin{proof}
	Assume there is one such edge \(e = (v, w)\), where \(w \in \lambda^{-}(v))\). Then in the next state \(w\) will become active. By Proposition \ref{prop:InactiveAncestorsProp} there can therefore exist no target attractor with \(v = 1\) since not all nodes in \(\lambda^{-}(v)\) are inactive. This completes the proof. 
\end{proof}

What we have now proven is that for a target attractor to exist for \(v\) with a target value of \(\alpha = 1\) all nodes in \(\lambda^{-}(v)\) must be inactive and furthermore that there can be no activation edge from \(v\) to any node in \(\lambda^{-}(v)\). It turns out that this generalizes to \(k\) nodes. That is if we have \(k\) nodes and we ask: is there an attractor where all of these \(k\) nodes are active? It can be solved in a similar way as for only one node but with an extra special case. The problem we now interested in solving is the following:

\begin{problem}
	{UnanimousAttractorMultiple}
	{An SRG(\(V, E\)) a set of target nodes \(V\) and target values \(\boldsymbol{\alpha} = \{1\}^{|V|}\).}
	{``Yes" if there exist a target attractor \(A\) with \(v = 1\), \(\forall v \in V\) . ``No" otherwise.}
\end{problem}

Proposition \ref{prop:InactiveAncestorsPropEdge} gives the condition for one node. We can extend it to \(k\) nodes by realizing that as soon as any node \(v\) has an activation edge into any other nodes \(u \neq v\) inactive ancestors, then there cannot exist such a target attractor that we are looking for. The reason is that this edge would violate Proposition \ref{prop:InactiveAncestorsPropEdge} since the node \(v\) is active and not inactive as required by the proposition. We can state this fact in many equivalent ways but one is the following:

\begin{prop}
	For there to exist a target attractor \(A\) for a set of nodes \(V\) with \(\boldsymbol{\alpha} = \{1\}^{|V|}\) no node can belong to any of the other nodes inactive ancestors. 
	\label{prop:ActiveAttractorMultiple}
\end{prop}

\begin{proof}
	Assume that some node \(v \in V\) belongs to some other nodes \(u\) inactive ancestors. By Proposition \ref{prop:InactiveAncestorsProp} no target attractor with the requested target values can then exist. This completes the proof.
\end{proof}

We have now covered the case for how to find a target attractor when the given target values are that all target nodes should be active. We now move on to the case when the target is instead that the node should be inactive.  

\subsection{Attractor with inactive target value}
We start this section with considering as before, the easier case when the target set \(V\) only consist of one node. The difference now is that we want the node to be inactive in the attractor, that is \(\alpha = -1\). We define a similar problem as we defined earlier when we considered active target values:

\begin{problem}
	{UnanimousAttractorSingleInactive}
	{An SRG(\(V, E\)) and a target value \(\alpha = -1\) and a target node \(v\).}
	{``Yes" if there exist a target attractor \(A\) with \(v = 1\). ``No" otherwise.}
\end{problem}

The thing to note with inactivity in the SRG model is that as soon as a node becomes inactive, its edges no longer have any impact on the nodes its connected to. Thus we can ignore the outgoing edges from our target node.

We start by defining the inactive ancestors for inactivity. An inactive node that is inactive can only become active if some other node activates it. For this reason the set of inactive ancestors for inactivity is the set of nodes that have a positive path to the target node. The inhibiting nodes will have no impact on the target nodes. Formally we define the set of \emph{inactive ancestors for inactivity} as follows:

\begin{definition} (Inactive ancestors for inactivity)
	The set of inactive ancestor for inactivity is defined as:
	
	\begin{equation*}
		\lambda^{+}(v) = \{u : \text{such that there is a positive path from \(u\) to \(v\)} \} 
	\end{equation*}
\end{definition}

Depending on the target value for a node we now have two different sets of inactive ancestors. For this reason we introduce the notation, \(\lambda(v)\) to mean the inactive ancestors of \(v\). Which set this refers to should be clear from context. Formally we define \(\lambda(v)\) as:

\begin{definition}(Inactive ancestors)
	The set of inactive ancestors for \(v\) given some target value \(\alpha \in \{1, -1\}\) is denoted by \(\lambda(v)\) and is defined as:
	
	\begin{equation*}
	\lambda(v) = \begin{cases}
	\lambda^{-}(v) & \text{if } \alpha = 1 \\
	\lambda^{+}(v) & \text{if } \alpha = -1.
	\end{cases}
	\end{equation*}
\end{definition}

In the rest of this section we will thus simply write \(\lambda(v)\) to mean the set of inactive ancestors. The following fact follows immediately by inspecting the graph and due to similar reasoning as for node activity in the previous section:

\begin{prop}
	For a target attractor \(A\) to exist with a target value \(\alpha = -1\) for a given node \(v\) it is enough that all nodes in the set \(\lambda(v)\) are inactive.
	\label{prop:singleInactiveAttracotr}
\end{prop} 

In fact such an attractor will always exist, consider the configuration \(\boldsymbol{v} = \{-1\}^{|V|}\), that is setting every node in the network to be inactive. By definition of our update function, all nodes will remain inactive if they do not have any kind of external stimuli. Proposition \ref{prop:singleInactiveAttracotr} actually gives something more powerful than simply stating that all nodes must be inactive. It says that it is enough that only the nodes belonging to the inactive ancestors need to be inactive. This allows for more freedom since we are still free choose the values for the other nodes. We will now prove this. 

\begin{proof}
	Consider a node that \(u\) that is not in the set of inactive ancestors for \(v\). This node will either only have a inhibition edge to \(v\) or it will reside in some other strongly connected component. Either case it will not have any effect on \(v\) once \(v\) has become inactive. For this reason we can ignore all such nodes \(u\).
	
	Consider now instead a node \(w\) that is in the set of inactive ancestors. Assume further that this node is not inactive. This non-inactive state will propagate trough the positive path and activate \(v\) in \(k\) timesteps where \(k\) is the length of the shortest positive path from \(w\) to \(v\). Thus we see that if any such node \(w\) is non-inactive then \(v\) will become active. For this reason all such nodes \(w\) must be inactive. 
\end{proof}

We now move on to the case when we have multiple target nodes that we want to have as inactive. In fact we can very easily extend Proposition \ref{prop:singleInactiveAttracotr} to multiple nodes in the following way. We simply require that for each node \(v\) in the target set, all nodes belonging to each node's inactive ancestors set must be inactive. The reasoning is simpler for this can than for the active case because as we mentioned we do not have to consider outgoing edges. We have now argued for the following thing:

\begin{prop}
	For a target attractor \(A\) given a target set of nodes \(V\) with target values \(\boldsymbol{a} = \{-1\}^{|V|}\) it is enough to set all nodes in \(\bigcup_{v \in V} \lambda^{+}(v)\) to inactive.
	\label{prop:InactiveAttractorMultiple}
\end{prop}

Now all that is left is to combine both of the cases for activity and inactivity and show that we can determine if a target attractor exist when we are given a combination of target values.

\subsection{Combining the cases}
In this section we want to combine the theory we developed in the previous two sections in order to find conditions that can solve \textbf{UnanimousAttractorDetection}. When combining the cases we are back to the original problem that we started with, namely Problem \ref{definition:targetAttractorDetection}.

It turns out that there are some additional cases we need to consider when we have a combination of target values. The first obvious thing is that Proposition \ref{prop:ActiveAttractorMultiple} must hold for all nodes that have a target value of \(\alpha = 1\). 

The second thing is that no node with a target value of \(\alpha = 1\) can be in the inactive ancestors for a node that has an inactive target value. The reason is that this would contradict Proposition \ref{prop:InactiveAttractorMultiple} since it requires that all nodes are inactive. When we are looking for a target attractor that has a combination of active and inactive values we will consider this as an extra case. The following theorem combines all that we have discussed thus far.

\begin{thm}
	For a target attractor to exist where the target nodes \(V\) have the following target values \(\boldsymbol{\alpha} = \{1, -1\}^{|V|}\), the following two conditions must hold:
	
	\begin{enumerate}
		\item For all nodes \(v \in V \) with a target value of \(\alpha = 1\), Proposition \ref{prop:InactiveAncestorsPropEdge} and Proposition \ref{prop:ActiveAttractorMultiple} must hold.
		\item For all nodes \(v \in V \) with a target value of \(\alpha = -1\). Proposition \ref{prop:InactiveAttractorMultiple} must hold. And no node \(u\) with an active target value can be in the inactive ancestors of \(v\). 
	\end{enumerate}
	\label{thm:SingletonAttractorMultiple}
\end{thm}

From this theorem we can see that this also gives a condition for periodic attractors to exist. Since if there there cannot be a singleton target attractor with the given target values then there certainly cannot be a periodic attractor that has those values. Thus we arrive at the following corollary:

\begin{cor}
	For a periodic target attractor to exist where the target nodes \(V\) have the following target values \(\boldsymbol{\alpha} = \{1, -1\}^{|V|}\), the conditions in Theorem \ref{thm:SingletonAttractorMultiple} must hold.
\end{cor}

In fact we have now shown that the original problem we set out to solve can be solved by applying the conditions stated in Theorem \ref{thm:SingletonAttractorMultiple}. What we have still not yet shown is that the solution is efficient. We will now give algorithms that put the theory into practice and then give a short analysis stating that the algorithms run in polynomial time, which we consider as an efficient runtime for an algorithm. 

\subsection{Algorithms}
The most important thing is that we should be able to compute the set of inactive ancestors, that is \(\lambda^{+}(v)\) and \(\lambda^{-}(v)\) efficiently. One way to do it is by starting from our target node \(v\), then compute in a breadth-first-search \cite{Skiena2008} like manner, the inactive ancestors. Here we consider instead of outgoing edges, the incoming edges. We have a set \(U\) where we keep track of all the nodes we have seen so far. We also have a queue where we put new nodes that we have not yet explored. When we encounter a node that we have not seen we put it into \(U\) as well as the queue. Then we keep exploring nodes from the queue until it is empty. Algorithm \ref{alg:Ancestors for inactivity} formalizes this idea.

 \begin{algorithm}
 	\begin{algorithmic}[1] % The number tells where the line numbering should start
 		\Procedure{InactiveAncestors}{$v$, $\alpha$}
 		\State Let $S$ be a queue
 		
 		\If{$\alpha = 1$}
	 		\State $U \gets \rho^{-}(v)$
	 	\Else
	 		\State $U \gets \emptyset$
	 	\EndIf
 		
 		\State $S$.enqueue($v$)
 		
 		\While{S is not empty}
 			\State $u \gets$ $S$.dequeue()
 			\For{$w \in \rho^{+}(u)$}
 				\If{$w \notin U$}
 					\State $U \gets U \cup \{w\}$
 					\State $S$.enqueue($w$)
 				\EndIf
 			\EndFor
 		\EndWhile
 		
 		\State \Return $U$
 	\EndProcedure
 	\end{algorithmic}
 	\caption{Algorithm for constructor the set of inactive ancestors. The parameter \(v\) is the target node and \(\alpha\) is the target value.}
 	\label{alg:Ancestors for inactivity}
 \end{algorithm}

Since Algorithm \ref{alg:Ancestors for inactivity} is a special case version of BFS we can immediately conclude that the worst case running time is \(\mathcal{O}(|V| + |E|)\), where \(V\) is amount of nodes in the network and \(E\) is the number of edges. We have now established that:

\begin{lemma}
	The set of inactive ancestors \(\lambda^{+}\) and \(\lambda^{-}\) can be constructed in \(\mathcal{O}(|V| + |E|)\) time. 
\end{lemma}

\begin{algorithm}
	\begin{algorithmic}[1] % The number tells where the line numbering should start
		\Procedure{UnanimousAttractor}{$T$, $\boldsymbol{\alpha}$}
		
		\State $T' \gets \{u_i : \alpha_i = 1 \}$
		\State $T'' \gets \{u_i : \alpha_i = -1 \}$
		
		\For{$0 \leq i \leq |T|$}
			\State $\lambda(v_i) \gets $ InactiveAncestors($v_i$, $\alpha_i$)
		\EndFor
		
		\For{$v \in T'$}  \Comment{Proposition \ref{prop:InactiveAncestorsPropEdge}}
			\If{There is an activation edge $(v, u)$, where $u \in \lambda(v)$}
				\State \Return $\mathcal{F}$
			\EndIf
		\EndFor
		
		\For{$v \in T'$} \Comment{Proposition \ref{prop:ActiveAttractorMultiple}}
			\For{$u \in T'\setminus\{v\}$}
				\If{$v \in \lambda(u)$}
					\State \Return $\mathcal{F}$
				\EndIf
			\EndFor
		\EndFor
		
		\For{$v \in T''$} \Comment{Inactivity requirement from Theorem \ref{thm:SingletonAttractorMultiple}}
			\For{$u \in T'$}
				\If{$u \in \lambda(v)$}
					\State \Return $\mathcal{F}$
				\EndIf
			\EndFor
		\EndFor
		
		\State \Return $\mathcal{T}$
		
		\EndProcedure
	\end{algorithmic}
	\caption{Algorithm that solves the \textbf{UnanimousAttractorDetection} problem. The parameters are the target set of nodes \(V\) and the target values \(\boldsymbol{\alpha}\).}
	\label{alg:TargetAttractorAlgorithm}
\end{algorithm}

Finally we will show an algorithm for solving \textbf{UnanimousAttractorDetection}. Algorithm \ref{alg:TargetAttractorAlgorithm} makes use of Theorem \ref{thm:SingletonAttractorMultiple} to decide whether the requested target attractor can exist. The algorithm returns either true or false, true means that the target attractor does exist while false means that no such attractor can exist. We can see that each condition that we derived for target attractors is checked in the algorithm. 

The running time of the algorithm is dominated by the time it takes to construct the set of inactive ancestors. The loop starting on line 4 can in the worst case run in \(\mathcal{O}|T|(|V| + |E|)\). This is because the time it takes to construct the inactive ancestors is \(\mathcal{O}(|V| + |E|)\) in the worst case. The other loops only works with smaller subsets of \(V\) and \(T\) so for that reason they do not have any impact on the worst-case running time. We have now shown the following result:

\begin{prop}
	Algorithm \ref{alg:TargetAttractorAlgorithm} has a worst case running time of \(\mathcal{O}(|T|(|V| + |E|))\) and solves \textbf{UnanimousAttractorDetection} in polynomial time. 
	\label{prop:TargetattractorRunTime}
\end{prop}

Using Proposition \ref{prop:TargetattractorRunTime} we can prove Theorem \ref{thm:UnanimousAttractorDetection} which we set as our goal for this section.

\UnanimousAttractorDetection*

\begin{proof}
	Proposition \ref{prop:TargetattractorRunTime} shows that Algorithm \ref{alg:TargetAttractorAlgorithm} runs in polynomial time and solves \textbf{UnanimousAttractorDetection}. 
\end{proof}

It is interesting to note there that the fact that  \textbf{UnanimousAttractorDetection} can be solved in polynomial time is due to how the dynamics of the SRG model is defined. If we compare this to the same problem in a BN context, then the same is probably not true. This is because in a BN \emph{any} Boolean function can be assigned to a given node so it is not possible to reason about the network structure and inactive ancestors in a similar way. 

To finish this section we will give an example to show how all the theory in this chapter is applied. 

\begin{example}
	Consider the SRG in Figure \ref{figure:cancerNetwork8Nodes}. Assume we are interested in finding out if there exists an attractor where the nodes \(V = \{E2F, Cdk2\}\) has the following target values \(\boldsymbol{\alpha} = \{1, -1\}\). We first compute the set of inactive ancestor for each of the nodes. The result is as follows:
	
	\begin{align*}
		\lambda^{-}(E2F) &= \{Myc, E2F, Cdc25A, Cdk2\} \\
		\lambda^{+}(Cdk2) &= \{E2F, Myc, Cdc25A\}
	\end{align*}
	
	Now we are ready to use Algorithm \ref{alg:TargetAttractorAlgorithm} to check all the conditions. The output is \(\mathcal{F}\) since the node \(E2F\) has an edge into its set of inactive ancestors. In fact there are multiple conditions that are not fulfilled for this target attractor to exist. One is that \(E2F\) is in the set of inactive ancestors for \(Cdk2\), which is not allowed. 
\end{example} 

In this section we have derived an algorithm for solving \textbf{UnanimousAttractorDetection} in polynomial time. Note that the algorithm does not give as output any such attractor it only decides if one can exist. In fact there can be multiple such target attractors. In some sense we do not really care about which of those target attractors we are in as long as the target values hold. Thus we would be interested in constructing at least one of the attractors. This we do in the next section.

\subsection{Constructing a target attractor}
In this section we build on the theory of the previous section and show an algorithm that constructs one of the target attractors given that one exists. The idea behind the algorithm is that the conditions for the target attractor gives some nodes their necessary values. This makes generating one of the attractors easier since we already have some of values for the nodes set. The target nodes needs to be given their target values. Apart from the target nodes and their inactive ancestors, there will be a set of nodes that have no impact on the dynamics of the target nodes. One possible solution is to simply initialize those nodes to some random values, then we simply simulate starting from that configuration until we reach an attractor. The set of nodes that have no impact on the target nodes, called \emph{non-impacting nodes} are defined as:

\begin{definition}(Non-impacting nodes)
	The set of \emph{non-impacting nodes}, denoted by \(\zeta(T)\), are nodes that have no impact on the target nodes. Meaning that there is no restriction on their values. They are all the nodes that are neither target nodes nor inactive ancestors. With this reasoning we can construct the set of non-impacting nodes as follows:
	
	\begin{equation*}
		\zeta(T) = V \setminus \left(T \cup \left[ \bigcup_{u \in T}(\lambda(u)) \right] \right)
	\end{equation*}
\end{definition}

We are now ready to present the algorithm for constructing a target attractor. 

\begin{algorithm}
	\begin{algorithmic}[1] % The number tells where the line numbering should start
		\Procedure{ConstructUnanimousAttractor}{$G(V, F), T, \boldsymbol{\alpha}$}
		
		\State $t \gets 0$
		
		\For{$v \in |T|$}
			$v(t) \gets \alpha_i$
			\For {$u \in \lambda(v)$}
				\State $u(t) \gets -1$
			\EndFor
		\EndFor
		
		\State $U \gets \zeta(T)$ \Comment{Nodes not effecting nodes in \(T\).}
		
		\For{$w \in U$}
			\State $w(t) \gets $ Random value from \(\{1, -1, 0\}\)
		\EndFor
		
		\State $G_{STG}(V', E') = \emptyset$
		
		\While{$G_{STG}$ is acyclic}
			\State $\boldsymbol{x}(t + 1) \gets F(\boldsymbol{x}(t))$
			\State $t \gets t + 1$
			\State $E' \gets E' \cup (\boldsymbol{x}(t), \boldsymbol{x}(t + 1))$
		\EndWhile
		
		\State \Return $G_{STG}$
		
		\EndProcedure
	\end{algorithmic}
	\caption{Algorithm that constructs a unanimous attractor. Assume the sets of inactive ancestors \(\lambda(v)\) has already been constructed. The parameters are the SRG \(G(V, F)\), the target nodes \(T\), and the target vector \(\boldsymbol{\alpha}\).}
	\label{alg:ConstructTargetAttractorAlgorithm}
\end{algorithm}

Algorithm \ref{alg:ConstructTargetAttractorAlgorithm} constructs one target attractor. Which attractor is constructed is due to random chance. The running time is dominated by the time it takes to construct the STG. This running time will depend on the time it takes before the simulation reaches the attractor. This time is called the \emph{transient period} of the system. At least for small works this period seems to be relatively short. 

We finish this section with an example on how to use Algorithm \ref{alg:ConstructTargetAttractorAlgorithm} to construct a target attractor.

\begin{example}
	Suppose that we are interested in finding a target attractor for the SRG in Figure \ref{figure:cancerNetwork8Nodes}, where our target set is \(T = \{pRb, p27\}\), and the target values are \(\boldsymbol{\alpha} = \{1, 1\}\). We first need the set of inactive ancestors for both of the nodes:
	
	\begin{align*}
		\lambda(pRb) &= \{Cdk2, E2F, Myc, Cdc25A, Cdk4\} \\
		\lambda(p27) &= \{Cdk2, E2F, Myc, Cdc25A\}
	\end{align*}
	
	From this we find the required values for all nodes but one. The nodes in \(\lambda(v) \cup \lambda(v)\) must all be inactive, while miR-17-92 can have any value. Lets say that it will be active. 
	
	Now we have a starting configuration \(\boldsymbol{x_0} = (1, -1, 1, -1, 1, -1, -1, -1)\). This configuration should be read in clockwise order from Figure \ref{figure:cancerNetwork8Nodes} starting from p27. After simulating from the state \(\boldsymbol{x_0}\) we have the following STG:
	
	\begin{equation*}
		(1, -1, 1, -1, 1, -1, -1, -1) \rightarrow (1, -1, -1, -1, 1, -1, -1, -1) \circlearrowleft
	\end{equation*}
	
	The arrow at the ends means that we stay in the same state. Now we have found one such target attractor that we were after. We can note here that letting miR-17-92 have a different initial value would have resulted in the same attractor. 
\end{example}