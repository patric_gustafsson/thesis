\chapter{Mathematical {P}reliminaries}
This chapter is a short introduction to the necessary mathematical structures and concepts that are used in this thesis. We start with a short overview of some basic concepts from algorithm analysis and complexity theory. Then we introduce the notion of Boolean networks, which is a widely used framework for modeling biological networks. After that we discuss some general issues about \emph{network controllability} and how that more specifically applies to discrete logic-based models such as Boolean networks. We also mention the complexity of some well-known problems related to Boolean networks and controllability.

\section{Algorithm analysis}
There is a need in computer science to categorize problems into classes so that it is possible to easily know if a problem is, in some sense, easy or difficult. In the field of algorithm analysis, we look closer at the performance of algorithms, often stated with pseudocode. We assume there is some abstract model of computation that can run this pseudocode and all operations take a set amount of time. Thus, we can ignore differences between different machines and architectures and focus only on what matters, which is the run-time and memory usage of our algorithms.

A useful tool for analyzing algorithms is the so-called ``Big-Oh" notation, popularized among computer scientists in the 1970's by Donald Knuth \cite{Knuth:1976}. The idea is that we can analyze algorithms in a machine-independent way. Constants and other factors are ignored even though they could have some difference in a real-life implementation. We only care about the operations that take the longest amount of time. An algorithm's running time is often defined as a function of the size of the input, which will be denoted by \(n\) in this section. 
 We can see that an algorithm that has a running time of \(n^2\) will be faster than one with \(2^n\) since already for small \(n\) the latter expression grows much faster. We formalize this idea as follows:

\begin{definition}(Upper bound)
	\(\mathcal{O}(f(n))\) is the set of all \(g(n)\) such that there exist constants \(c \ge 0\) and \(n_0 \ge 0\) with the property that \(|g(n)| \leq cf(n)\), for all \(n \ge n_0\).
\end{definition}

If we say that \(g(n) = \mathcal{O}(f(n))\), then we can intuitively think about it as \(f\) being a strict upper bound on \(g\), meaning that when \(n \ge n_0\), \(f\) will always grow faster than \(g\). There is a similar notion for a function that works as a lower bound:

\begin{definition}(Lower bound)
	\(\Omega(f(n))\) is the set of all \(g(n)\) such that there exist constants \(c \ge 0\) and \(n_0 \ge 0\) with the property that \(|g(n)| \geq cf(n)\), for all \(n \ge n_0\).
\end{definition}

Similarly to the upper bound we can now think of \(g(n) = \Omega(f(n))\) as saying that \(g(n)\) always grows faster than \(f(n)\) when \(n \ge n_0\). Combining the definitions we have a definition for a strict bound:

\begin{definition}(Exact bound)
	\(\theta(f(n))\) is the set of all functions \(g(n)\) such that \(g(n) = \Omega(f(n))\) and \(g(n) = \mathcal{O}(f(n))\).
\end{definition}

Here we see that the statement \(g(n) = \theta(f(n))\) implies that \(g(n)\) grows at exactly the same rate as \(f(n)\). 

When doing algorithm analysis we are often interested in the worst-case running time. We often try to find an upper bound for some algorithm and state that in the worst case, the algorithm will always have a running time below some upper bound. It is often easy to find a simple upper bound, yet finding an exact one can be more challenging. In this thesis, we will deal mostly with bounds that are quadratic or exponential. Quadratic means that the running times grow by the square of the input, such as \(n^2\) while exponential means that there is some exponential relationship with the input, such as \(2^n\).

\section{Computational complexity}
The goal of computation complexity theory is to categorize different problems into classes, where each class represents a different degree of difficulty. The field is relatively new compared to other fields of science. It began to manifest itself among researches in the 1970's due to Karp's paper \cite{karp1972reducibility} where he showed that 21 well-known problems related to graph-theory were in fact \(\mathcal{NP}\)-complete.  A recent textbook about computational complexity which this section is based on is \cite{arora2009computational}. Although there exists many complexity classes, we will only focus on the two perhaps most famous ones, \(\mathcal{P}\) and \(\mathcal{NP}\). Informally, the difference between these two classes can be explained by an example.

Consider the problem called the \emph{Traveling salesman}-problem. The goal of the salesman is to visit every city on a given map only once and, in addition, he wants to return to his original starting city. Now, given a list of visited cities \((c_1, c_2, \cdots, c_n)\) it is easy to verify that he indeed visited every city once and that \(c_1 = c_n\). However, without this list how should we propose a route for him to take? There exists an enormously large number of different paths. The total number of routes are \(n!\) in the case of a fully connected graph. The class of \(\mathcal{NP}\) can be described as problems where given a solution, it is easy to verify that solution, but finding a solution is much more difficult. Formally, complexity classes are often defined in terms of a Turing Machine (TM). The reason is that the TM is the simplest form of computation that is still equivalent to most other notions of a computational device.

We first define the class of \(\mathcal{P}\) in terms of decision problems. A decision problem is a problem that has a \emph{yes} or \emph{no} answer. The problem of determining whether a number is prime or not is a well-known decision problem. 

\begin{definition} (Decision Problem)
	A decision problem is a set \(L_f = \{x : f(x) = 1\}\), where \(f\) is a Boolean Function, giving 0 or 1 as an answer. The problem then is, given an instance \(x\) of a problem, decide if \(f(x) = 1\) or equivalently \(x \in L_f\). 
\end{definition}

We look at Boolean functions more closely in the next section in the context of Boolean networks. Here it is only relevant to know that they give either 0 or 1 as an output.  

\begin{definition} (Class \(\mathcal{P}\))
	The class of \(\mathcal{P}\) is the set of all decision problems that can be decided in polynomial time, that is \(\mathcal{O}(n^k)\), for some \(k \ge 0\). 
\end{definition}

Problems that belong to \(\mathcal{P}\) are usually thought of as problems that can be solved efficiently. We now move on to the class of \(\mathcal{NP}\). In modern literature this class is often defined in terms of a verifier, which we also use here. The intuition behind the verifier was explained in the traveling salesman example above. That is, given a solution to a problem, we can easily check that the solution is correct. 

\begin{definition} (Class \(\mathcal{NP}\))
	The class of \(\mathcal{NP}\) is the set of all decision problems that has a verifier that runs in polynomial time. That is, given a solution \(y\) to a problem \(L_f\), the verifier outputs:
	\begin{enumerate}
		\item ``Yes" if \(y\) is a solution, that is, \(y \in L_f\);
		\item ``No" if \(y\) is not a solution, that is, \(y\ \notin L_f\).
	\end{enumerate}
\end{definition}

Note that the definition does not include any information about actually obtaining the solution. From the definition we see that \(\mathcal{P} \subseteq \mathcal{NP}\) since having an algorithm that produces a solution in polynomial time is essentially our verifier for problems in \(\mathcal{P}\). Problems that are in \(\mathcal{NP}\) are often thought of as problems where finding a solution that runs in polynomial time is impossible. Within \(\mathcal{NP}\) there is a class of problems known as \(\mathcal{NP}\)-complete problems. They are defined as follows:

\begin{definition} \((\mathcal{NP}\)-completeness)
	A decision problem \(X\) is said to be \(\mathcal{NP}\)-complete if 
	\begin{enumerate}
		\item \(X\) is in \(\mathcal{NP}\);
		\item  Every other problem in \(\mathcal{NP}\) is reducible to \(X\) in polynomial time.
	\end{enumerate}
\end{definition}

Reducible essentially means that we have some algorithm that can transform an instance of a problem \(X\), into another instance of a problem \(Y\), in polynomial time. A common way to show that a specific problem \(Y\) is \(\mathcal{NP}\)-complete is to reduce another \(\mathcal{NP}\)-complete problem \(X\) to \(Y\). If we can find a polynomial time solution to any problem that is \(\mathcal{NP}\)-complete, then we have found a solution to every problem in \(\mathcal{NP}\) and this would imply that \(\mathcal{NP} = \mathcal{P}\). Most researchers are of the opinion that \(\mathcal{NP} \neq \mathcal{P}\) \cite{fortnow2009status}, but no proof of this exists.

The following famous problem is an example of a \(\mathcal{NP}\)-complete problem \cite{karp1972reducibility}.

\begin{problem}
	{3SAT}
	{A logical formula \(L\) that consists of \(k\) clauses and where each clause \(C_i\) contains a disjunction of three variables.}
	{``Yes" if there exists an satisfying assignment to the formula. ``No" otherwise.}
\end{problem}

\begin{example}
	The following is an instance of 3SAT:
	
	\begin{equation*}
		(x_1 \vee \neg x_2 \vee x_3) \wedge (x_4 \vee x_5 \vee \neg x_6).
	\end{equation*}
	
	The above formula has two clauses, each with three variables. This instance has a solution. The partial assignment \((x_1, x_4) = (1, 1)\) satisfies the formula.
\end{example}

Reducing from 3SAT is a common way to prove \(\mathcal{NP}\)-completeness of some problems. The reason is that many problems can be naturally encoded as a binary choice which correspond to assigning the value of 1 or 0 to a variable \(x_i\) in the formula for a 3SAT instance. 

\section{Boolean networks}
This section is a short review of terms needed for working with Boolean networks. Boolean networks were introduced in 1969 by Kauffman \cite{KAUFFMAN1969}. A Boolean network is a graph where each node can be either expressed or not corresponding to the Boolean values of true and false, denoted respectively by \(0\) and \(1\). A more recent text on Boolean networks is \cite{AkutsuBNAlgos} and includes the theory on which this chapter is based.   

Each node in the network has an activation function. For Boolean networks this function is a Boolean function. A Boolean function take as inputs Boolean variables and gives a Boolean output, 0 or 1.

\begin{definition}{(Boolean function.)}
  A Boolean function with \(n\) inputs is a function \(f(x_1, \cdots, x_n) : \{0, 1\}^n \rightarrow \{0, 1\} \). 
\end{definition}
Using vectors the notation can be simplified. Let \(\boldsymbol{x} = (x_1, \cdots, x_n)\) where each \(x_i\) is a Boolean variable. Then a Boolean function \(f(x_1, \cdots, x_n)\), can be written as \(f(\boldsymbol{x})\).

All Boolean functions can be constructed by using one or more of the \emph{fundamental} Boolean functions. The fundamental Boolean functions are:
\begin{enumerate}
\item The constant function \(f = 0 \text{ and } f = 1\);
\item The identity function \(f(x) = x\), maps an element to itself;
\item Negation \(f(x) = \neg x\), flips the truth values;
\item Conjunction \(f(x, y) = x \wedge y\);
\item Disjunction \(f(x, y) = x \vee y\);
\item Implication \(f(x, y) = x \rightarrow y\).
\end{enumerate}

\begin{example}
  Consider the following Boolean function \(f(x, y, z) = (x \vee (y \wedge z))\). This function has three Boolean variables \(x, y, \text{ and } z\). It uses two of the fundamental Boolean functions. The assignment \(\boldsymbol{a_1} = (1, 0, 1)\) gives \(f(\boldsymbol{a_1}) = 1\), while the assignment \(\boldsymbol{a_2} = (0, 0, 1)\) gives \(f(\boldsymbol{a_2}) = 0\).
\end{example}

We are now able to define a Boolean network formally.

\begin{definition}(Boolean Network) A Boolean network (BN), is a graph \(G\), with a set of nodes \(V = \{x_1, x_2, \cdots , x_n\}\), and a list of Boolean functions \(F = (f_1, f_2, \cdots, \\ f_n)\). The list of functions defines the edges. If a node \(x_j\) appears in the formula for a node \(x_i\), then there is an edge from \(x_j\) to \(x_i\). The edges are directed, so pairs of edges are ordered, that is \((x_i, x_j)\) is an edge from \(x_i\) to \(x_j\). We will denote BNs in this text as \(G(V, F)\), where \(V\) is a set of nodes and \(F\) is a list of Boolean functions, one for each node.
	\label{definition:BN}
\end{definition}

We illustrate the definition by showing an example of a BN that we will as a running example in this section. 

\begin{example}
	Consider a BN that consists of the following three nodes \(x_1, x_2,\) and \(x_3\), then \(V = \{x_1, x_2, x_3\}\). To know which nodes have edges between them, we need to define a Boolean function for each node. Assume they are defined as follows: 
	\begin{align*}
		\begin{split}
			x_1(t + 1) &= x_1(t) \vee \neg x_3(t) \\
			x_2(t + 1) &= x_1(t) \wedge x_3(t) \\
			x_3(t + 1) &= x_2(t).
		\end{split}
	\end{align*}	
	
	From this equation, we can now state the edges of the BN. Recall from Definition \ref{definition:BN}, that there is an edge from node \(x_i\) to \(x_j\) if \(x_j\) appears in the formula for \(x_i(t)\). Following this reasoning we see that the set of edges is \(E = \{(x_1, x_1), (x_1, x_2), (x_2, x_3), (x_3, x_1), (x_3, x_2)\} \). The whole BN can be seen in Figure \ref{figure:ExampleGraphBoolean}.
	
	\label{example:BooleanGraphExamle}
\end{example}

\section{Dynamical systems of Boolean networks}
By considering how a BN evolve over time we get a \emph{discrete dynamical system}. This system has only a finite amount of states, since each Boolean variable can only take on two values. For a network consisting of \(n\) nodes, we have \(2^{n}\) different states the network can be in.  We now go trough two well-known updating schemes called the \emph{synchronous} and the \emph{asynchronous} updating scheme \cite{Netzwerke2009DynamicsOB}. 

\begin{enumerate}
	\item \emph{Synchronous}. This model was introduced in \cite{KAUFFMAN1969} in 1969. Its main feature is that all nodes are updated simultaneously. This is the model that we will focus on in this thesis. Mainly because it has been studied extensively and is simpler to reason about than the \emph{asynchronous} model. Even tough the synchronous update rule can be seen as simple from a biological point of view, it has been used recently to predict oncogenesis, which is how cells turns into cancer cells \cite{fumia2013boolean}. Another use case is development of personalized medicine \cite{zanudo2018discrete}. 
	
	\item \emph{Asynchronous}. In the \emph{asynchronous} model the nodes are updated in a random order and at different timescales. The way this is usually done is to have a permutation \(\pi\), and apply this to the vector \((x_1, \cdots, x_n)\) which gives \((x_{\pi(1)}, \cdots x_{\pi(n)})\). A new permutation is generated each timestep and \(x_{\pi(i)}\) means that node \(x_i\) gets updated as the \(i:\)th node in that round of updates. 
	This model was introduced in \cite{thomas1991regulatory}. The idea of the model being that there is more choice at each state and should therefore be able to model more complex interactions, were the interactions can be non-deterministic, which is usually the case in biological systems. Since each state is no longer exactly determined by the previous state, there will be multiple ways to traverse the state space of the dynamics. Reasoning about the dynamics becomes more difficult in the asynchronous case, since updating is no longer completely deterministic. 
\end{enumerate}

In this thesis we only use the synchronous updating scheme, however the asynchronous one is useful to know about since they are closely related.

\subsection{Formal definition of synchronous updating}

Now we formally define how to reason about the dynamics of a BN.  

\begin{definition}{(Configuration)}
  First denote the number of nodes in a given BN by \(m\), then a \emph{configuration} of a BN, is a vector \[\boldsymbol{x} \in \{1, -1, 0\}^{m}.\] That is, \(\boldsymbol{x} = (x_1, x_2, \ldots, x_m)\) where \(x_i \in \{1,-1,0\}\) for all \(1 \leq i \leq m\). A configuration describes the state of a BN completely at any time \(t\). 
\end{definition}

Each timestep \(t\) the network changes configuration. A \emph{transition} in the network from time \(t\) to \(t + 1\) is given by:
\begin{equation*}
  \boldsymbol{x}(t + 1) = \boldsymbol{f}(\boldsymbol{x}(t)). 
\end{equation*}
Here \(\boldsymbol{x} = (x_1, x_2, \ldots, x_m)\) and \(\boldsymbol{f} = (f_1, f_2, \ldots, f_m)\), where each \(f_i\) is a Boolean function assigned to each node. A transition for a single node \(x\) is written as \(x(t +1) = f(\boldsymbol{x})\). To visualize how the dynamics of the systems evolve over time we can use a \emph{state-transition table} (STT) and a \emph{state-transition graph} (STG). In the STT we enumerate all the states the BN can be in, each row includes a \(\boldsymbol{s} \in \{1,-1,0\}^{m}\) and the corresponding next state: \(f(\boldsymbol{s})\). 

The STG is a graph where each node is one of the states \(\boldsymbol{s}\), and there is an edge between two nodes if both states appear at the same row in the STT. The number of nodes in the STG, and the number of rows in the STT, are the same \(2^{m}\), since we enumerate each state.

\begin{example}
	Consider the same network as in Example \ref{example:BooleanGraphExamle}. In Table \ref{table:STTExampleBooleanNetwork}, we have created the STT for the BN. We can see that since the network consists of three nodes, that the total amount of rows in the STT is \(2^3 = 8\). 
\end{example}

\begin{figure}
  \centering
  \begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}]
	
    % Nodes
    \node[main node] (A) {$x_1$};
    \node[main node] (B) [right = of A] {$x_2$};
    \node[main node] (C) [below = of A] {$x_3$};
    
    \draw [->] (A) [loop above] edge (A);
    \draw [->] (A) edge (B);
    \draw [->] (B) [bend right] edge (C); 
	\draw [->] (C) edge (A);
	\draw [->] (C) [bend right] edge (B);
  \end{tikzpicture}
  \caption{Boolean network with three nodes. Adapted from \cite{wang2012boolean}.}
  \label{figure:ExampleGraphBoolean}
\end{figure}

\begin{table}	
	\centering
	\begin{tabular}{c  c  c | c  c  c}
		\(x_1(t)\) & \(x_2(t)\) & \(x_3(t)\) & \(x_1(t+1)\) & \(x_2(t+1)\) & \(x_3(t+1)\) \\
		\hline
		0 & 0 & 0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 1 & 0 & 1 \\
		0 & 1 & 1 & 0 & 0 & 1 \\
		1 & 0 & 0 & 1 & 0 & 0 \\
		1 & 0 & 1 & 1 & 1 & 0 \\
		1 & 1 & 0 & 1 & 0 & 1 \\
		1 & 1 & 1 & 1 & 1 & 1  
	\end{tabular}
	\caption{The state-transition diagram for the BN in Figure \ref{figure:ExampleGraphBoolean}. We can see that it has \(8 = 2^3\) rows. One for each configuration of the network.}
	\label{table:STTExampleBooleanNetwork}
\end{table}

\subsection{Attractors and their basins}
Starting from an initial configuration in a BN one of the following two things are bound to happen: either a fixed point will be reached in which the network will stay, or it will encounter a previously seen node and stay in a cycle of those nodes. The fixed points and cycles of states are called \emph{attractors} of the BN. An attractor is a set \(\{A_1, A_2, \cdots, A_n  \}\), where each state follows from the previous one. Except when \(i = n\), then we ``wrap around" to the first state. Formally we can state this as:

\begin{definition}(Attractor)
	An attractor of a BN is a set of states
	
	\begin{equation*}
		\{A_1, A_2, \cdots, A_n\},
	\end{equation*}
	
	where each \(A_i\) for \(2 \leq i \leq n - 1\) can be calculated as:
	
	\begin{equation*}
		A_i = \boldsymbol{f}(A_{i - 1}).
	\end{equation*}
	
	The first entry \(i = 1\) is calculated in a special way \(A_1 = \boldsymbol{f}(A_n)\). This is so that we ``wrap around" correctly and complete the cycle.
\end{definition}

The size of the attractor set is called the \emph{period} or length of the attractor. The attractors that have period \(p = 1\) are called \emph{singleton attractors}, and those that have period \(p \geq 1\) are called \emph{periodic attractors}. Note that singleton attractors are just a special case of the periodic attractors. In this thesis, when we mention periodic attractors we will always mean those that have \(p \geq 2\), as to distinguish them from the singleton attractors. 

Another way to define attractors is as cycles in the STG. Each connected component of the graph contains one attractor. The set of states that eventually lead to an attractor is called the \emph{basin} of the attractor. There exist different interpretations of how these attractors correspond to real biological functions of the cell. One interpretation \cite{huang1999gene}, is that singleton attractors correspond to cell differentiation or apoptosis. Cell differentiation means that the cell becomes more specialized at doing a certain task. Apoptosis is programmed cell death \cite{elmore2007apoptosis}, it is considered a vital part of many normal human processes such as embryo development, however it can also be a factor in neurodegenerative diseases, as well as many types of cancers, if an abnormal amount of apoptosis take place. 

A periodic attractor could correspond to different phases of the cell cycle. During various phases of the cell cycle, similar patterns of gene activity can be seen in the cell as in the periodic attractor, thus making this a compelling interpretation. We now show an example of some attractors.

\begin{example}
	The attractors of the BN from Example \ref{example:BooleanGraphExamle} can be found by creating the STG. To create the STG, we first have to create the STT. For each row, we create a new node that corresponds to that state. We then add an edge between the node for the state at time \(t\) and the state at time \(t + 1\). Note, that if the two states are the same, then we have found a singleton attractor. Using this method, we find that the BN has two singleton attractors: \(\{(1, 1, 1)\}\), \(\{(1, 0, 0)\}\), and also a periodic attractor: \(\{(1,0,1), (1,1,0)\}\)
\end{example}

One observation is that if the basin of attraction is large, then many mutations can occur in the initial state of the cell and it will still end up in the same stable state at the end. The largest attractor in the network can thus be seen as the normal function of the cell, and considerable mutation is needed in order to reach any of the other stable states, assuming that they have a much smaller basin of attraction \cite{huang1999gene}. Even though it is generally assumed that the most important cell states manifest themselves as attractors with the largest basin, this is not always the case. In \cite{espinosa2004gene}, they argue that some attractors which have small basins, can be important biologically. 

From these arguments, it follows that we would often be interested in knowing what kind of attractors there are in our network. The most obvious way to find them is to simply enumerate the whole STG. This is only possible for small networks (\(n \leq ~20\)), since the STG grows exponentially, on the order of \(\Omega{(2^n)}\). Consider now the problem of determining if a given BN contains a singleton attractor:

\begin{problem}
	{SingletonAttractor}
	{Given a BN \(G(V, F)\), determine if it has a singleton attractor.}
	{``Yes" if the BN contains a singleton attractor. ``No" otherwise.}
\end{problem}

\begin{prop}
	\textbf{SingletonAttractor} is \(\mathcal{NP}\)-complete, if we restrict the indegree of each node to at most \(k\). 
\end{prop}

\begin{proof}
	The proof can be found in \cite{AkutsuBNAlgos}.
\end{proof}

The reason we restrict the indegree to some \(k\), is that the problem is not \(\mathcal{NP}\)-complete for unrestricted \(k\). In the case of unrestricted \(k\), the problem becomes \(\mathcal{NP}\)-hard. 

We might also be interested in finding not only singleton attractors, but also periodic attractors. The problem can be stated formally as:

\begin{problem}
	{PeriodicAttractor}
	{A BN \(G(V, F)\), and a period \(p\).}
	{``Yes" if the BN has a periodic attractor with period \(p\), ``No" otherwise.}
\end{problem}

The complexity of this problem is not known. It is believed to be \(\mathcal{PSPACE}\)-complete \cite{AkutsuBNAlgos}. In both cases, singleton and periodic, we should not expect any polynomial time algorithms to be available. Since attractor finding is closely related to the satisfiability problem, some algorithms try to take advantage of that fact. One such algorithm is based on model checking and SAT-solvers \cite{dubrova2011sat}. It is capable of handling some hundreds of nodes. A drawback of the algorithm is that it only seems effective for networks that have their indegree restricted to some \(k \leq 2\). 

\section{Network controllability}
Consider a dynamical system that has entered an unhealthy state. This could, for example, be a cell or some small system inside the cell. What we would be interested in, is to guide or \emph{control} the network away from the unhealthy state to a healthy one. The \emph{control} in controllability, refers to the fact that we take control of some nodes and control their states trough different mechanisms. A common approach is to choose a subset of the nodes, called driver nodes, formally denoted by \(D\) \cite{hou2016finding}.

These driver nodes can be chosen in multiple ways. They can either be nodes from the original network, then \(D \subseteq V\), or they are extra nodes added to the network. If the nodes are chosen from the original graph, then they are called \emph{internal nodes}, otherwise we call them \emph{external nodes} \cite{akutsu2007control}. 

The most general form of controllability states that we should be able to guide the network from any initial state into any final state. Formally we can state this as:

\begin{problem}
	{AnyStateControl}
	{A BN \(G(V, F)\), an initial state \(\boldsymbol{x_0}\), and a final state \(\boldsymbol{x_m}\).}
	{A minimal set of driver nodes that can steer the BN from the initial state to the final state in \(m\) timesteps.}
\end{problem}

\begin{prop}
	AnyStateControl is \(\mathcal{NP}\)-hard.
\end{prop}

\begin{proof}
	The proof can be found in \cite{hou2016finding}.
\end{proof}

However, instead of allowing the final state to be an arbitrary state, it might be more reasonable to restrict the target state to attractors \cite{hou2019number}. As mentioned earlier, attractors often correspond to biologically significant states. For this reason, we also focus on controllability to attractors in this thesis. This perspective has the other advantage that it is enough to only reach the \emph{basin} of the attractor that we are interested in, since by reaching the basin we will eventually fall into the attractor. This gives us more possible states as control targets and still reach the attractor. Another good reason to consider control methods when the target is an attractor, is due to a recent result regarding controllability to attractors that was shown in \cite{hou2019number}. They showed, that if we limit controllability to only attractor states, then the expected number of driver nodes for a given BN is \(\mathcal{O}(\log_2{m} + \log_2{n})\), where \(n\) is the number of nodes and \(m\) is the number of attractors in the network. This is a significant result, since even with large networks the expected number of attractors is small for BNs. We now state two problems that are related to the controllability problems we will consider when introducing our own model.

\begin{problem}
	{AttractorDependentControl}
	{A BN \(G(V, F)\), an initial state \(\boldsymbol{x_{0}}\), an attractor \(A\).}
	{A minimal set of driver nodes that can drive the network from the initial target state into the attractor.}
\end{problem}

\begin{problem}
	{AttractorIndependentControl}
	{A BN \(G(V, F)\), an initial state \(\boldsymbol{x_{0}}\).}
	{A minimal set of driver nodes that can drive the network to any attractor in the network.}
\end{problem}

In the first problem, we have an initial state and want to go from that state to the attractor. The second problem is a more general version, where we do not have a specific attractor as the goal, rather we want to find a set of nodes such that they allow us to control the network into any attractor. For each of these two problems, it is enough that we simply reach the basin of the target state. The complexity of these problems, is to the author's best knowledge, still unknown. After introducing our own model, we will in the next chapter take a look at some special controllability problems in the context of our model.






