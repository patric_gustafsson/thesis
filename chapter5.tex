\chapter{Description of the {S}imulation {S}oftware}
To be able to study the dynamics of our model and see long term behavior in non-trivial networks software assistance is required. In this chapter we give a description of the requirements for the software, an overview of its architecture, and lastly a breakdown of the different features available. The software is available to download from \url{https://bitbucket.org/patric_gustafsson/network-sim/src/master/}, and can be run on Windows, Mac and Linux based systems. The software does not require any additional libraries or packages in order to be run.
We use the cancer model that we presented in Section \ref{section:cancer_model} as a running example to show the features of the software. 

\section{Requirements}
The requirements of the software evolved from the problems we wanted to study. Our models become computationally difficult to compute by hand even when the number of nodes \(n\) is very small. Already \(n = 4\) has \(3^4 = 81\) states. So we can see that already for small models it becomes a necessity to have access to software that can handle the simulation automatically. 

The software was developed using methods similar to Agile methods and has for that reason no set requirements at the start. Rather we developed iteratively different prototypes to see what would best fit our needs at the time. This is the essence of Agile, to be able to cope with changing requirements quickly and develop rapid prototypes in order to always have a working version of the software \cite{manifesto2001manifesto}. Some requirements were known from the start tough:

\begin{enumerate}
	\item \emph{Simulation}. The software should possess the capability to run simulations on a predefined network. The results, which is the whole STG should be available as a file in some appropriate format. The file should be compatible with other graph visualization software as to ease the analysis of the STG. Since the STG can be very large it needs to be opened in tools that have the capability to handle large graphs such as Cytoscape \cite{shannon2003cytoscape}. 
	
	\item \emph{Attractors}. The software should be able to compute the attractors using whatever algorithms we come up with, or simply with brute force. They should then be presented inside the software to the user. 
	
	\item \emph{Controllability}. The user should be able to study controllability of a given network inside the software. From an initial state the user should be able to see the dynamics evolve and be able to specify the state of some nodes or make them constant. 
\end{enumerate}

These requirements have all been implemented and in the next section we give an overview of the software's architecture and see how the requirements have shaped some of the design choices. 

\section{Architecture of the software}
The software follows the Model-View-Controller (MVC) pattern which is a common pattern for developing desktop GUI applications and was first introduced in the 1980s in \cite{krasner1988description}. The idea with MVC is to separate the logic, data, and the viewing of the software data into different classes or structures so that they are independent from each other. Here is a short breakdown of the main responsibilities for each component in MVC:

\begin{description}
	\item [Model] The models purpose is to hold the data that the application needs that is domain-specific. A good example from \cite{krasner1988description} is that the model of a text editor would be a string. For our software it would be the graph that is loaded in by the user. The model is also responsible for doing all the computations.
	
	\item [View] The view is what the user sees when interacting with the program. The view should issue calls to the controller whenever some features that needs the model is used. In our software the view would be the GUI that is presented to the user when the program is started. Whenever the view is updated it has to request the most recent data from the model.
	
	\item [Controller] The controller is responsible for communication between the model and the view. In our software we do not explicitly use a controller class and communication is mainly done between the model and the view. Since Java automatically handles button events for us there is no need for a separate controller for doing that functionality. The controller is in some form still there in our software only that is handled by existing Java classes.
\end{description}

In Figure \ref{figure:ClassDiagram} the structure of our application is shown. The \emph{MainApplication} class is the view of the application. It contains functions for updating the GUI and make heavy use of the \emph{NetworkSim} class which actually does all the computations. The \emph{NetworkSim} class is in this sense our model since it takes care of computations and keeps our model, the graph, as a class member. Whenever the view wants to update it has to request the graph from the \emph{NetworkSim}. We then also have two additional helper classes that are used. One is the \emph{DynamicsAnimator} which takes care of animating the updates done to the network when using the controllability feature of the software, which we will explain later. For reading the actual data from disk, a class called \emph{MVGParser} is used. 

For the actual simulation we use a library called BioLQM developed by the Colomoto (Consortium for Logical Models and Tools) Consortium \cite{Naldi2018BioLQMAJ}. It is a Java library for manipulation of logical models and includes features for doing various simulations on the model. In our software it is used by the class \emph{NetworkSim} to simulate our models. BioLQM was chosen since it has ready made support for multi-valued models. The function \emph{exportToMnet} creates a file that can be read and interpreted by BioLQM. 

One modification had to be made to BioLQM before we could use it in our simulator. BioLQM has multi-value logic support but not in the manner that we need. BioLQM uses the multi-valued notion that we discussed in Section \ref{sec:ComparisonToOtherModels} where values during the simulation could only be updated in a monotonous manner. This is not how the SRG model works rather updates can happen from any value to any other value. After we modified BioLQM to use the kind of update that we wanted we successfully included it into our software.   

\begin{figure}
	\begin{tikzpicture}[]
		\begin{class}[text width = 7cm]{MainApplication}{-2, 0}
			\attribute{loadedModel : File}
			\attribute{networkSimulator : NetworkSim}
			\attribute{dynamicsAnimator : DynamicsAnimator}
		
			\operation{loadModel()}
			\operation{simulateModel()}
			\operation{findAttractorsBruteForce()}
			\operation{showTableForAttractors()}
			\operation{animateDynamics()}
		\end{class}
	
		\begin{class}[text width = 9cm]{NetworkSim}{-2 ,-10}
			\attribute{model : LogicalModel}
			\attribute{MVGParser : MVGParser}
			\operation{loadModel(file : File)}
			\operation{simulateModel(resultFileName : String) : String}
			\operation{findAttractorsBruteForce() : List\textless List\textless String\textgreater{}\textgreater}
			\operation{generateBoolNetFile()}
		\end{class}
		
		\begin{class}[text width = 5cm]{MVGParser}{-2, -14}
			\operation{read(filePath : Path)}
			\operation{exportToMnet(mnetFilePath : Path)}
			\operation{getGraph() : Graph}
		\end{class}
		
		\begin{class}[text width = 8cm]{DynamicsAnimator}{0, -6}
			\attribute{networkSimulator : NetworkSim}
			\operation{startAnimation(animSpeed : int, initState : byte[])}
			\operation{stopAnimation()}
			\operation{continueAnimation()}
		\end{class}
	
		\aggregation{[xshift=-3cm]MainApplication.south}{}{}{[xshift=-3cm]NetworkSim.north}
		\aggregation{NetworkSim}{}{}{MVGParser}
		\aggregation{DynamicsAnimator}{}{}{[xshift=2cm]NetworkSim.north}
		\aggregation{[xshift=2cm]MainApplication.south}{}{}{DynamicsAnimator}
	\end{tikzpicture}
	\caption{Overall class structure of our simulation software. Only the methods that are relevant to the discussion are included in the diagram. They represent the main functionality of the application and features that we discuss.}
	\label{figure:ClassDiagram}
\end{figure}
\section{Features of the software}
In this section we give an overview of what features are available in the software. We also provide screenshots of the main functionality which can function as a short manual for using the software. Before any simulations can be done a network must first be loaded in. We have created a file format called MVG (Multi-valued Graph) for this purpose. The format is simple and has a restricted grammar so that it can easily be parsed and used. 

\subsection{The MVG format}
The format can be seen as a subset of the simple interaction format (SIF) which is used by Cytoscape. The grammar is simple and is best understood by an example.

\begin{example}
	
	\begin{lstlisting}
	
		A <node interaction> B
		B <node interaction> C
		C <node interaction> D E F
		E <node interaction> F
	\end{lstlisting}
	
	On the left hand side of the <node interaction> should be the name of the first node in the interaction. The right hand side should be the name of the node that is being interacted with. Node names are unique so a node can only have one name. It is also possible to specify multiple interactions at once as is done on line three. Interactions where a node is interacting with themselves are also allowed, this will give self-loops.
\end{example}

As mentioned the MVG format is a subset of SIF. With SIF you can specify many possible interactions but we are only really interested in the network topology. That is why we define \(\text{<node interaction>} ::= \{``+" | ``-"\}\). This statement should be read in extended Backus-Naur form (EBNF) \cite{pattis2013ebnf}. Here a ``+" means that there is a regulatory activation between the nodes. That is if A is active and there is no contradiction then B should become active. Likewise for ``-". Next we show an example on how to use the MVG format.

\begin{example}
	The cancer network from Figure \ref{figure:cancerNetwork8Nodes} would be written in the following way in the MVG format: 
	\begin{lstlisting}
		Myc + Mir1792
		Myc + E2F
		Myc + Cdc25A
		Myc + Cdk2
		Myc - Myc
		Mir1792 - E2F
		Mir1792 - Mir1792
		E2F + E2F
		E2F + Myc
		E2F + Cdc25A
		E2F + Cdk2
		E2F + E2F
		E2F + Mir1792
		pRb - E2F
		Cdk2 + Cdc25A
		Cdk2 - p27
		Cdk2 - pRb
		Cdc25A - Cdc25A
		Cdc25A + Cdk2
		Cdk4 - Cdk4
		Cdk4 - pRb
		p27 - Cdk2
	\end{lstlisting}
\end{example}

\subsection{Viewing a network}
A network can be loaded in and viewed in the software by clicking on \emph{File} and choosing \emph{Load Network} from the menu. A MVG file should then be chosen that is written in the format described in the previous section. There is no upper limit on the number of nodes that the network can consist of. The software has been tested with a few hundred nodes. See Figure \ref{fig:ViewNetwork} for a screenshot of the software after we have loaded in the cancer network. 

When the network has loaded the nodes and the edges between the nodes should be visible. It is possible to move nodes around by clicking and dragging them. Edges can also be moved in a similar manner. A green edge means activation and a red edge means inhibition. If a new network is loaded in then the previous network will be deleted and the new one will be displayed instead. The software tries to layout all the nodes in a circular fashion, this is the reason for the nodes being in a circle whenever a network is loaded in.

\begin{figure}
	\centering
	\includegraphics[scale=0.65]{SoftwareScreenshot/viewingNetwork.PNG}
	\caption{Screenshot of the software just after having loaded in a model of the cancer network from Section \ref{section:cancer_model}.}
	\label{fig:ViewNetwork}
\end{figure}

Under the menu \emph{Tools} there exist a few different things that can be done with the network. In the following sections we describe each of them.

\subsection{Simulating a network}
By clicking on \emph{Tools} and then choosing \emph{Simulate Network} the network will be simulated. By simulate we here mean that we generate each state that the network can be in and simulate to the next state. Essentially constructing the whole STG. After the simulation is done the program will output a Comma-Separated Value (CSV) file, which contains two columns, one for the initial state and one for the successor state. The CSV file can be imported and viewed into programs such as Cytoscape that are specifically made to work with large graphs. 
There is a limit for how large the network can be before the memory requirements become too large to handle. In testing on a computer with 8GB of RAM, about \(n = 25\) is the upper limit before the JVM and the computer runs out of memory. 

\subsection{Computing attractors}
The software also provides methods for computing attractors. Currently only a brute-force method is available. It can be invoked by going into \emph{Tools} and choosing \emph{Compute Attractors}. The brute-force algorithm works by generating the whole STG in which it then looks for cycles. Every cycle in the STG is an attractor. This method suffer from the same limitations as the simulation feature, since it is essentially doing the same thing and also searching for attractors. After the computation is done the attractors will be shown in the tool in a new window, see Figure \ref{fig:ComputeAttractors}. The software will also open a window where the attractors are ranked by the size of their basins. This is useful since the STG quickly grows so large that it is infeasible to show it for larger networks. Thus knowing the size of each basin of attraction can help to give some sort of birds eye view over the state space without explicitly drawing it, this window can be seen in Figure \ref{fig:ComputeAttractors}. 

\begin{figure}
	\subcaptionbox{Top three attractors shown in the tool.}[.3\linewidth]	 {
		\includegraphics[scale=0.6]{SoftwareScreenshot/computeAttractors.PNG}
	}
	\subcaptionbox{Tabular view of the attractors. Ranked by their basin size.} {
		\includegraphics[scale=0.80]{SoftwareScreenshot/AttractorTable}		
	}
	\caption{Two screenshots from the software. On the left we see the top three attractors. On the right we can see a tabular view of the attractors ranked by their basin size.}
	\label{fig:ComputeAttractors}
\end{figure}

\subsection{Booleanization of a network}
The algorithm presented in section \ref{booleanizatinSection} is available in the software. It takes the network and generates an output file that can then be imported into the R program BoolNet \cite{mussel2010boolnet}. Boolnet is an R library that contains many different tools and ready implemented algorithms for working with Boolean networks, so it would not make sense to reimplemented them into our software. This method can be run even on very large networks since the BN that is created contains only double the amount of nodes that the original network contains and the algorithm itself runs in quadratic time in the worst case. 

\subsection{Controlling a network}
Controllability can also be studied with the software. By selecting \emph{Tools} and then \emph{Simulate Dynamics}, a dialog opens where an initial state can be entered. After that an animation starts on the network in the view. Nodes that are active will be displayed in green, inactive ones in red, and ambiguous ones will be in a dark gray color. The animation can be paused by clicking on the \emph{Pause} button on the right hand side in the GUI. It should now be possible to click on each node and change its state, thus controlling it, and then resuming the simulation. See Figure \ref{fig:ControllabilityTable} for a demonstration of this feature.  

\begin{figure}
	\centering
	\subcaptionbox{The network shown in the controllability view. Here the initial state has been chosen as having all nodes active.} {
		\includegraphics[scale=0.70]{SoftwareScreenshot/controllability1}	
	} \\
	\subcaptionbox{The network shown after one timestep. At this point it is possible to stop the simulation and set different values for some nodes if needed and then resume the simulation. } {
		\includegraphics[scale=0.70]{SoftwareScreenshot/controllability2}		
	}
	\caption{The controllability view is shown here. In a) we see the network in the chosen initial state. In b) we see the network after one timestep from the initial state.}
	\label{fig:ControllabilityTable}
\end{figure}

