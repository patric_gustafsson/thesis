\section{Boolean network transformation} \label{booleanizatinSection}
In this section we present an algorithm for transforming any network expressed in our model into an equivalent Boolean one. Since we are interested in controllability we will consider the two networks equivalent if the attractors of the two networks have a one-to-one mapping between them. Because if the attractors were somehow different then any results found by this transformation would not be useful since we would not be reasoning about the same state space anymore. The usefulness of this transformation is mostly in the controllability sense. Since there has been some work on controllability in BNs it would be useful for us to be able to apply those results to the SRG model as easily as possible. Transforming a given SRG into a BN is one way to achieve this. Another benefit that we can mention is that the transformation will provide us with an upper bound for many complexity problems. As we know, many attractor and controllability related problems are \(\mathcal{N}\mathcal{P}\)-hard in a BN context. This reduction would thus give an upper bound on \textbf{UnanimousAttractorDetection}, since it is a special case of \textbf{PeriodicAttractor} which as mentioned earlier, is believed to be \(\mathcal{PSPACE}\)-complete.

To start of this section we will first give a high-level intuitive explanation of how the transformation is done and in what sense the newly created BN is equivalent to the SRG it was derived from. After that we will dive into the theory and state the transformation in more formal terms. We finish this section by presenting the complete transformation algorithm and show that it can be done in polynomial time. 

\subsection{High-level description}
The intuitive idea behind the algorithm is that we only need two bits to encode our three value logic with binary states. With two bits we can represent \(2^2 = 4\) different states so our three states can be encoded. The next step then comes from looking at the update function in Definition \ref{definition:NodeUpdateShort}. As can be seen each update clause is essentially a logical statement that has a binary outcome. They are also disjunct from each other. In this sense we will set one node to be the ``active" node \(\hat{v}\) and another to be the ``inactive" node \(\tilde{v}\). The node \(\hat{v}\) will have the update function that corresponds to the state ``1" in our model, similarly we assign to the node \(\tilde{v}\)  the update function that corresponds to ``-1". The state ``0" is modeled in the binary case implicitly by the fact that if both \(\hat{v}\) and \(\tilde{v}\) updates to false then this corresponds to the \emph{otherwise} update clause in our model. Thus each time none of the nodes are true then the original node \(v\) would be updated to ``0" in the SRG model.
The notion of activating and repressing edges are translated implicitly in the Boolean formula for each new node as well. This new BN that we create there will only have ``normal" unlabeled edges. 

In Figure \ref{fig:BNTransformIntuitive} we show graphically the idea behind the transformation. We can see that the node \(v\) is split into two new nodes, \(\hat{v}\) and \(\tilde{v}\). Here \(\hat{v}\) is the ``active" node and \(\tilde{v}\) is the ``inactive" node. We also see that all other nodes have been split into two as well. Since we have not yet defined a Boolean function for each we should not draw any edges between the nodes since those are dependent on the nodes in each of the functions, still we jump slightly ahead here and show the edges for illustration purposes. We will see later that our transformation will create exactly such a function that gives rise to such edges. 
 
\begin{figure}
	\subcaptionbox{Arbitrary node \(v\) in a SRG before the transformation.}[.35\linewidth][c] {
		\begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}, minimum size=0.90cm]
		% Nodes
		\node[main node] (A) {$v$};
		\node[main node] (B) [above left = of A] {$x_1$};
		\node[main node] (C) [above right = of A] {$x_{k1}$};
		\node[main node] (D) [below left = of A] {$z_1$};
		\node[main node] (E) [below right = of A] {$z_{k2}$};
			
		\path[every node/.style={font=\sffamily\small}]
		(B) edge [-Bar, color=inhibit] node [] {} (A)
		(C) edge [-Bar, color=inhibit] node [] {} (A)
		(D) edge [color=active] node [] {} (A)
		(E) edge [color=active] node [] {} (A);
			
		\path (B) -- node[auto=false]{\ldots} (C);
		\path (D) -- node[auto=false]{\ldots} (E);
		\end{tikzpicture}\vspace{0.45cm}}
	\subcaptionbox{The same node \(v\) but now after the transformation. We can see \(v\) has now been split into two nodes \(\hat{v}\) and \(\tilde{v}\). Every other nodes has also been split into two.}[.6\linewidth][c] {
		\begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}, minimum size=0.90cm]
		
		% Nodes
		\node[main node] (A1) {$\hat{v}$};
		\node[main node] (A2) [right = of A1] {$\tilde{v}$};
		
		% Inhibtion nodes
		\node[main node] (B1) [above= of A1] {$\widetilde{x_1}$};
		\node[main node] (B2) [left = of B1] {$\widehat{x_1}$};
		\node[main node] (C1) [above = of A2] {$\widehat{x_{k1}}$};
		\node[main node] (C2) [right = of C1] {$\widetilde{x_{k1}}$};
		
		% Activation nodes
		\node[main node] (D1) [below = of A1] {$\widetilde{z_1}$};
		\node[main node] (D2) [left = of D1] {$\widehat{z_1}$};
		\node[main node] (E1) [below = of A2] {$\widehat{z_{k2}}$};
		\node[main node] (E2) [right = of E1] {$\widetilde{z_{k2}}$};
		
		\path[every node/.style={font=\sffamily\small}]
		(A1) edge [->] node [] {} (A2)
		(A2) edge [->] node [] {} (A1)
		
		% Edges from inhibtion nodes
		(B1) edge [] node [] {} (A1)
		(B1) edge [] node [] {} (A2)
		(B2) edge [] node [] {} (A1)
		(B2) edge [] node [] {} (A2)
		(C1) edge [] node [] {} (A1)
		(C1) edge [] node [] {} (A2)
		(C2) edge [] node [] {} (A1)
		(C2) edge [] node [] {} (A2)
		
		% Edges from activation nodes
		(D1) edge [] node [] {} (A1)
		(D1) edge [] node [] {} (A2)
		(D2) edge [] node [] {} (A1)
		(D2) edge [] node [] {} (A2)
		(E1) edge [] node [] {} (A1)
		(E1) edge [] node [] {} (A2)
		(E2) edge [] node [] {} (A1)
		(E2) edge [] node [] {} (A2)
		
		% Edges between each node also
		(B1) edge [] node [] {} (B2)
		(B2) edge [] node [] {} (B1)
		(C1) edge [] node [] {} (C2)
		(C2) edge [] node [] {} (C1)
		(D1) edge [] node [] {} (D2)
		(D2) edge [] node [] {} (D1)
		(E1) edge [] node [] {} (E2)
		(E2) edge [] node [] {} (E1);
		
		\node at ($(B2)!.5!(C2)$) {\ldots};
		\node at ($(D2)!.5!(E2)$) {\ldots};
		\end{tikzpicture}}
	\caption{On the left we see a node in the SRG before the transformation. The picture on the right demonstrates how the node \(v\) and its immediate adjacent nodes will look like in the BN after the transformation.}
	\label{fig:BNTransformIntuitive}
\end{figure}

\subsection{Formalization}
Consider a node \(v\) in the SRG. In the BN that we create here \(v\) will correspond to two new nodes, \(\hat{v}\) and \(\tilde{v}\). The following encoding scheme is used for the active, inactive and ambiguous states:

\begin{equation}
	\begin{cases}
		(\hat{v}, \tilde{v}) = (1, 0) \implies \text{Active} \\
		(\hat{v}, \tilde{v}) = (0, 1) \implies \text{Inactive} \\
		(\hat{v}, \tilde{v}) = (0, 0) \implies \text{Ambiguous}
	\end{cases}
	\label{eqn:BooleanEncoding}
\end{equation}

In Equation \eqref{eqn:BooleanEncoding} we can see the encoding. The idea is that the \(\hat{v}\) node only has the value 1 when the node is active in the SRG. Same idea for the \(\tilde{v}\) node. Now if neither of these are 1 then this should correspond to the ambiguous state as explained earlier. This is encoded by \((0, 0)\). We can see that \((1, 1)\) is unused but it does not matter since we only have three values to encode. Assuming that the formulas we present later are correct, we can decode a state from the transformed BN back into the SRG model by using a decoding function. We formally define it as follows:

\begin{definition}(Decoding Function)
	The \emph{decoding function} is a function 
	
	\begin{equation*}
		\pi : \{1, 0\}^{|2V|} \rightarrow \{1, -1, 0\}^{|V|}
	\end{equation*}
	
	where \(|V|\) is number of nodes in the given SRG under consideration. The decoding function is defined as:
	
	\begin{equation}
		\pi(\boldsymbol{v}) = (\tau(v_1, v_2), \ldots, \tau(v_{2|V| - 1}, \tau(v_{2|V|})).
	\end{equation}
	
	Here \(\tau\) is a function \(\tau: \{1, 0\} \times \{1, 0\} \rightarrow \{1,-1,0\}\) and is defined as:
	
	\begin{equation}
		\tau(v_1, v_2) = \begin{cases}
							1, & (v_1, v_2) = (1, 0) \\
							-1, & (v_1, v_2) = (0, 1) \\
							0 & (v_1, v_2) = (0, 0) 
						\end{cases}
	\end{equation}
\end{definition}

Using the decoding function we can easily decode states from the transformed BN back into our SRG model. We make a small note here that the transformed BN will have \(3^{|2V|}\) states since we are introducing two new nodes for each node in the SRG. This means that the state space of the BN will be larger that our original state space for the SRG and thus include some states that have no meaning in the SRG framework. We will simply ignore these states since they would have no meaning in the SRG anyway. 

Lets now take a look at how we actually define the Boolean functions that lets us create an equivalent BN to our SRG. If we look in the update rules in Table \ref{table:NodeUpdateTable}, we can see that we need to check the following conditions, one from each column in the table:

\begin{equation}
	\begin{cases}
		\rho^{+}(v) \subseteq \{-1\} & \text{``All adjacent activation nodes are inactive"} \\
		\rho^{-}(v) \subseteq \{-1\} & \text{``All adjacent inhibtion nodes are inactive"} \\
		1 \in \rho^{+}(v) & \text{``At least one adjacent activation node is active"} \\
		1 \in \rho^{-}(v) & \text{``At least one adjacent inhibition node is active"} \\
		\rho^{+}(v) \subseteq \{-1, 0\} & \text{``All adjacent activation nodes are inactive or ambiguous"} \\
		\rho^{-}(v) \subseteq \{-1, 0\} & \text{``All adjacent inhibition nodes are inactive or amibguous"} \\
	\end{cases}
	\label{eqn:UpdateCases}
\end{equation}

In Equation \eqref{eqn:UpdateCases} we can see that we need to encoded quantified statements with logical formulas. We first consider the simpler question of how to write logical formulas for these same statements when we have only one node. Now instead of checking if all nodes are for instance, active, we instead consider only one node. The states we then want to formulate according to Equation \eqref{eqn:UpdateCases} are: \emph{a node is active}, \emph{a node is inactive}, and \emph{a node is inactive or ambiguous}. The first statement can be written quite easily as follows:

\begin{equation}
	\hat{v} \wedge \neg \tilde{v}. 
	\label{eqn:ActiveBNNode}
\end{equation}

We can see that the formula in Equation \eqref{eqn:ActiveBNNode} is only true if \((\hat{v}, \tilde{v}) = (1, 0)\) which is exactly how we encode the active node in Equation \eqref{eqn:BooleanEncoding}. We can now construct a similar formula for a node that is inactive:

\begin{equation}
	\neg \hat{v} \wedge \tilde{v}.
	\label{eqn:InactiveBNNode}
\end{equation}

Again, Equation \eqref{eqn:InactiveBNNode} will only be true if \((\hat{v}, \tilde{v}) = (0, 1)\) which is how we encoded inactivity earlier. Now only the last statement remains. We want to make a logical formula for when a node is either inactive or ambiguous. We already know how to check if a node is inactive from Equation \eqref{eqn:InactiveBNNode}, so we only need to combine that with the formula for a node being ambiguous. We combine them using a disjunction since either of the states can be true:

\begin{equation}
	(\neg \hat{v} \wedge \tilde{v}) \vee (\neg \hat{v} \wedge \neg \tilde{v}).
	\label{eqn:InactiveOrAmbigousNodeBN}
\end{equation}
We see that the first part of the disjunction is just Equation \eqref{eqn:InactiveBNNode}. The right part \(\neg \hat{v} \wedge \neg \tilde{v}\) is only true when \((\hat{v}, \tilde{v}) = (0, 0)\) which again is how we encoded the ambiguous state. Equation \eqref{eqn:InactiveOrAmbigousNodeBN} can be simplified to a more intuitive form:

\begin{align*}
	(\neg \hat{v} \wedge \tilde{v}) \vee (\neg \hat{v} \wedge \neg \tilde{v}) 
	& \equiv (\neg \hat{v} \wedge (\tilde{v} \vee \neg \tilde{v})) && \text{(De Morgan)} \\
	& \equiv (\neg \hat{v} \wedge T) && \text{(Law of Excluded Middle)} \\
    & \equiv \neg \hat{v}.
\end{align*}

The whole formula has collapsed into simply \(\neg \hat{v}\). This is a quite natural result since the formula now simply says \emph{\(v\) is not active}, which is equivalent to saying: \emph{\(v\) is either inactive or ambiguous} that we had before. Now we can state them all together at once:

\begin{equation*}
	\begin{cases}
		\hat{v} \wedge \neg \tilde{v} & \text{\(v\) is active}; \\
		\neg \hat{v} \wedge \tilde{v} & \text{\(v\) is inactive}; \\
		\neg \hat{v} & \text{\(v\) is not active}.
	\end{cases}
\end{equation*}

We now have the logical formulas we need in order to create Boolean functions that correspond to the statements in Equation \eqref{eqn:UpdateCases}. The equation has six statements, one for activation and for inhibition each. We only cover one of each sort since they are symmetric to each other. 
If we first consider the statement ``All adjacent activation nodes are inactive", that is \(\rho^{+}(v) \subseteq \{-1\}\). This statement is quantified over all adjacent nodes, so we might think that we would need quantifiers such as \(\forall\) in the formula. This is not the case since the set of adjacent nodes are finite for each node. Quantified statement are essentially infinite conjunctions but since our domain is limited we can stay within propositional logic for the formulas. The following formula makes sure that all adjacent nodes are inactive:

\begin{equation*}
	\bigwedge_{w \in \rho^{+}(v)} (\neg \widehat{w} \wedge \widetilde{w}).
\end{equation*}

What is important to note here is that \(v\) is a node in the SRG while \(\widehat{w}\) and \(\widetilde{w}\) are nodes in the new BN that we are creating. The next statement is "At least one adjacent activation node is active". What this is saying is that, as soon as we have a active node, this statement should be true. A natural choice for this is to have a disjunction over all adjacent nodes together with the activation formula from Equation \eqref{eqn:ActiveBNNode}. We get the following:

\begin{equation*}
	\bigvee_{w \in \rho^{+}(v)} (\widehat{w} \wedge \neg \widetilde{w}).
\end{equation*}

The last of the three statements is: ``All active nodes should be either inactive or ambiguous" or as we derived earlier: ``All active nodes should be non-active", which is a bit more of a mouthful to say in text, but simpler with logic. The formula is then by the same reasoning as the two earlier cases:

\begin{equation*}
	\bigwedge_{w \in \rho^{+}} (\neg w).
\end{equation*}

We now have formulas for the BN that are equivalent to the statements in Equation \eqref{eqn:UpdateCases}. Now the only matter that is left is to combine them so that they correspond to Table \ref{table:NodeUpdateTable}. Between each column is a conjunction and between the two rows there is a disjunction. The rule for inactivity, the first two rows of the table, can then be stated as a Boolean function that we assign to the ``inactive" node \(\tilde{v}\):

\begin{equation}
	\begin{split}
	\tilde{v}(t) &= \left( \bigwedge_{w \in \rho^{+}(v)} (\neg \widehat{w} \wedge \widetilde{w}) \right)  \wedge 
				 \left(\bigvee_{u \in \rho^{-}(v)} (\hat{u} \wedge \neg \bar{u}) \right)
				 \vee \\
				  &\quad \left(\bigwedge_{w \in \rho^{+}(v)} (\neg \widehat{w} \wedge \widetilde{w}) \right) \wedge \left(\bigvee_{u \in \rho^{-}(v)} \neg \hat{u} \right) \wedge
				  (\neg \hat{v} \wedge \tilde{v}) .
	\end{split}
	\label{eqn:InactiveBNFormulaFull}
\end{equation}

We can see that Equation \eqref{eqn:InactiveBNFormulaFull} that it fully encodes the rules for a node to become inactive. For completeness we will also state the same rule for the active node \(\hat{v}\). 

\begin{equation}
	\begin{split}
	\hat{v}(t) &= 
		\left(\bigvee_{u \in \rho^{+}(v)} (\hat{u} \wedge \neg \tilde{u}) \right) \wedge 
		\left( \bigwedge_{w \in \rho^{-}(v)} (\neg \widehat{w} \wedge \widetilde{w}) \right)
			\vee \\
		&\quad   
		\left(\bigvee_{u \in \rho^{+}(v)} \neg \hat{u} \right) \wedge \left(\bigwedge_{w \in \rho^{-}(v)} (\widehat{w} \wedge \neg \widetilde{w}) \right) \wedge
		(\hat{v} \wedge \neg \tilde{v}).
	\end{split}
	\label{eqn:ActiveBNFormulaFull}
\end{equation}

Using \cref{eqn:InactiveBNFormulaFull,eqn:ActiveBNFormulaFull} we now have for each node a function to assign to it. Using all this we can now describe the complete algorithm for doing the Booleanization. We describe it in the next section.

\subsection{Algorithm} 
In this section we present the complete algorithm for doing the Booleanization. We also discuss its complexity. See Algorithm \ref{alg:Booleanization} for the complete algorithm written in pseudocode. 

\begin{algorithm}
	\caption{Booleanization of an SRG}
	\label{euclid}
	\begin{algorithmic}[1] % The number tells where the line numbering should start
		\Procedure{Booleanize}{$G, G_B$} \Comment{Construct a BN $G_B$ from the graph $G$}
		\State $V \gets G(V)$
		\State $V_B \gets G_B(V)$
		
		\For{$v \in V$}
			\State $V_B \gets V_B \cup (\hat{v}, \tilde{v})$
			\State $\hat{v}(t) \gets $ Equation \eqref{eqn:ActiveBNFormulaFull}
			\State $\tilde{v}(t) \gets $ Equation \eqref{eqn:InactiveBNFormulaFull}
		\EndFor
		\State \Return $G_B$
		\EndProcedure
	\end{algorithmic}
	\label{alg:Booleanization}
\end{algorithm}

As can be seen from the code, the algorithm constructs a new BN called \(G_B\) that has for every node \(v \in G\) two new nodes in \(G_B\). This means that \(|V_B| = 2|V|\), meaning that there is double the amount of nodes in \(G_B\) compared to \(G\). The edges are defined in row 6 and 7 by \cref{eqn:InactiveBNFormulaFull,eqn:ActiveBNFormulaFull}. If we look closer on the formulas we see that if \(|\rho^{+}(v)| > 0\), then we need to add two edges to both nodes \(\hat{v}\) and \(\tilde{v}\). Same applies when \(|\rho^{-}(v)| > 0\). The final clause in each of the equations also adds a self-loop to each node and an edge from \(\hat{v}\) to \(\tilde{v}\) and vice versa. If we sum over the indegree of each node in \(G_B\) then we will get the total number of edges. Denote the indegree of a node \(u \in V_B\), by \(\delta^{+}(u)\). This quantity can be calculated as:

\begin{equation}
	\delta^{+}(u) = 2 \left(|\rho^{+}(v)| + |\rho^{-}(v)| \right) + 2, \forall u \in V_B.
	\label{eqn:NodeIndegreeBN}
\end{equation} 

To get the total amount of edges we simply sum over the indegree of all the nodes.

\begin{equation*}
	|E_B| = \sum_{u \in V_B} \delta^{+}(u)
\end{equation*}

We can see that in general \(G_B\) will be a very densely connected graph with a high indegree for each node. As can be seen from Equation \eqref{eqn:NodeIndegreeBN} we see that \(\delta^{+}(u) \geq 2\) since the node \(u\) will always have a self loop and at least one more edge from its partner node. 
Finally we also mention the complexity of Algorithm \ref{alg:Booleanization}. We will prove that it runs in quadratic time with respect to the number of nodes in the SRG. 

\begin{prop}
	Algorithm \ref{alg:Booleanization} has a worst-case running time of \(\mathcal{O}(|V|^2)\).
\end{prop}

\begin{proof}
	 We see that the running time is dominated by the loop starting on line four. Inside the loop we construct each of the Boolean functions according to the given equations. To do this we have to go trough each edge in \(\rho^{+}(v)\) and \(\rho^{-}(v)\). In the worst case, which is if the graph \(G\) is fully connected with respect to both type of edges, then the size of these two sets will be \(|V|\), including self-loops. What this means is that in the worst case we have to for each node \(v \in V\) consider every other node in \(V\setminus\{v\}\). The following calculation shows this formally.  
	
	\begin{align*}
		\mathcal{O}(|V|\cdot(|\rho^{+}(v)| + |\rho^{-}(v)|) \nonumber
		& = \mathcal{O}(|V|\cdot(|V| + |V|) \nonumber && |\rho^{+}(v)| = |\rho^{-}(v)| = |V| \\
		& = \mathcal{O}(2|V|^{2}) && \nonumber \\ 
		& = \mathcal{O}(|V|^2) 
		\label{eqn:AlgorithmBNComplexity}
	\end{align*}
	
	Thus we have now shown that Algorithm \ref{alg:Booleanization} has a worst-case running time of \(\mathcal{O}(|V|^2)\).
\end{proof}

This result shows that the transformation can be done in polynomial time and is thus efficient. Next we show an example that brings some of the theory of this chapter to life. 

\begin{example}
	Consider the graph in Figure \ref{figure:ExampleGraph3Nodes}. If we give this graph to Algorithm \ref{alg:Booleanization} it returns the graph that can be seen in Figure \ref{figure:BNTransformExampleGraph}. Lets also look at each of the Boolean functions since they are what actually define the edges in the BN. The following functions were constructed using the equations we derived earlier. 
	
	\begin{align*}
		\begin{split}
			%x_1s
			\widehat{x_1}(t) & = ((\neg \widehat{x_2} \wedge \widetilde{x_2}) \wedge (\neg \widehat{x_3} \wedge \widetilde{x_3})) \vee	((\widehat{x_2} \wedge \neg \widetilde{x_2}) \wedge (\widehat{x_3} \wedge \neg \widetilde{x_3}) \wedge (\widehat{x_1} \wedge \neg \widetilde{x_1}))
			\\
			\widetilde{x_1}(t) & = ((\widehat{x_2} \wedge \neg \widetilde{x_2}) \vee (\widehat{x_3} \wedge \neg \widetilde{x_3})) \vee ((\neg \widehat{x_2}) \vee (\neg \widehat{x_3}) \wedge (\neg \widehat{x_1} \wedge \neg \widetilde{x_1}))
			\\
			% x_2s
			\widehat{x_2}(t) & = ((\widehat{x_1} \wedge \neg \widetilde{x_1}) \wedge (\neg \widehat{x_3} \wedge \widetilde{x_3})) \vee ((\neg \widehat{x_1}) \wedge (\widehat{x_3} \wedge \neg \widetilde{x_3}) \wedge (\widehat{x_2} \wedge \neg \widetilde{x_2}))
			\\
			\widetilde{x_2}(t) & = ((\neg \widehat{x_1} \wedge \widetilde{x_1}) \wedge (\widehat{x_3} \wedge \neg \widetilde{x_3})) \vee ((\neg \widehat{x_1} \wedge \widetilde{x_1}) \wedge (\neg \widehat{x_3}) \wedge (\neg \widehat{x_2} \wedge \widetilde{x_2}))
			\\
			\widehat{x_3}(t) & = ((\widehat{x_1} \wedge \neg \widetilde{x_1}) \vee (\widehat{x_2} \wedge \neg \widetilde{x_2})) \vee ((\neg \widehat{x_1} \vee \neg \widehat{x_2}) \wedge (\widehat{x_3} \wedge \neg \widetilde{x_3}))
			\\
			\widetilde{x_3}(t) & = ((\neg \widehat{x_1} \wedge \widetilde{w}) \wedge (\neg \widehat{x_2} \wedge \widetilde{x_2})) \vee ((\neg \widehat{x_1} \wedge \widetilde{x_1}) \wedge (\neg \widehat{x_2} \wedge \widetilde{x_2}) \wedge (\neg \widehat{x_3} \wedge \widetilde{x_3}))
		\end{split} 
	\end{align*}
	As we can see from the equation above every node is connected to every other node. 
	\label{example:BNTransformExample}
\end{example}

As the last thing that we cover in this section, we show an example how the equivalence between the attractors work in practice. Remember that the equivalence is not only in the attractors but also in the transitions between the states.

\begin{example}
	The SRG in Figure \ref{figure:ExampleGraph3Nodes} has the attractor that can be seen in Figure \ref{figure:BNTransformExampleSTG} along with its basin. The BN constructed in Example \ref{example:BNTransformExample} has the ``same" attractor and basin that can also be seen on the right in Figure \ref{figure:BNTransformExampleSTG}. The states from the BNs STG can be decoded by using the decoding function we defined earlier. The states would be decoded as follows:
	
	\begin{align*}
		\pi((0,0,0,1,1,0)) &= (\tau(0, 0), \tau(0,1), \tau(1,0)) = (0, -1, 1) \\
		\pi((0,1,0,0,1,0)) &= (\tau(0, 1), \tau(0,0), \tau(1,0)) = (-1, 0, 1) \\
		\pi((0,1,0,1,0,0)) &= (\tau(0, 1), \tau(0,1), \tau(0,0)) = (-1, -1, 0)
	\end{align*}
	We can see that the decoded attractors are the same as ones that are found in the SRG. 
\end{example}

The transformation that we have presented in this section while certainly useful, is of more theoretical interest than practical. The blowup of the number of edges in the transformed BN is likely an obstacle for many current algorithms that work with controllability problems on BNs. It can still be used for a modest number of nodes and if any really efficient algorithms for controllability problems for BNs are ever invented they can be applied directly to our SRG model with the help of this transformation.

\begin{figure}
	\centering
	\subcaptionbox{Attractor and its basin in the SRG.}[.25\linewidth][c] {
		\begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}]
		% Nodes
		\node[main node, rectangle] (A) {$0, -1, 1$};
		\node[main node, rectangle] (B) [below = of A] {$-1, 0, 1$};
		\node[main node, rectangle] (C) [below = of B] {$-1, -1, 0$};
		
		\path[every node/.style={font=\sffamily\small}]
		(A) edge [->] node [] {} (B)
		(B) edge [->] node [] {} (C)
		(C) edge [->, loop below] node [] {} (C);
		\end{tikzpicture}}
	\subcaptionbox{Attractor and its basin in the transformed BN.}[.4\linewidth][c] {
		\begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}]
		% Nodes
		% BN STG
		\node[main node, rectangle] (D) {$0, 0, 0, 1, 1, 0$};
		\node[main node, rectangle] (E) [below = of D] {$0, 1, 0, 0, 1, 0$};
		\node[main node, rectangle] (F) [below = of E] {$0, 1, 0, 1, 0, 0$};
		
		\path[every node/.style={font=\sffamily\small}]
		(D) edge [->] node [] {} (E)
		(E) edge [->] node [] {} (F)
		(F) edge [->, loop below] node [] {} (F);
		\end{tikzpicture}}
	\caption{Figure showing the two different paths in the STG from the SRG  in Figure \ref{figure:ExampleGraph3Nodes} and the BN created from the SRG in Example \ref{example:BNTransformExample}. On the left is the SRGs STG and on the right is the BNs STG.}
	\label{figure:BNTransformExampleSTG}
\end{figure}

\begin{figure}
	\centering
	\begin{tikzpicture}[->, auto, node distance=1cm, main node/.style={circle,draw}]
	% Nodes
	\node[main node] (A1) [] {$\widetilde{x_1}$};
	\node[main node] (A2) [right = of A1] {$\widehat{x_1}$};
	\node[main node] (B1) [above left = of A1] {$\widetilde{x_2}$};
	\node[main node] (B2) [above = of B1] {$\widehat{x_2}$};
	\node[main node] (C1) [above right = of A2] {$\widetilde{x_3}$};
	\node[main node] (C2) [above = of C1] {$\widehat{x_3}$};
		
	\path[every node/.style={font=\sffamily\small}]
	
	% Loops between the corresponding nodes
	(A1) edge [->] node [] {} (A2)
	(A2) edge [->] node [] {} (A1)
	(B1) edge [->] node [] {} (B2)
	(B2) edge [->] node [] {} (B1)
	(C1) edge [->] node [] {} (C2)
	(C2) edge [->] node [] {} (C1)
	
	% Edges betwwen the other nodes
	(A1) edge [->] node [] {} (B1)
	(A1) edge [->] node [] {} (B2)
	(A1) edge [->] node [] {} (C1)
	(A1) edge [->] node [] {} (C2)
	
	(A2) edge [->] node [] {} (B1)
	(A2) edge [->] node [] {} (B2)
	(A2) edge [->] node [] {} (C1)
	(A2) edge [->] node [] {} (C2)
	
	(B1) edge [->] node [] {} (A1)
	(B1) edge [->] node [] {} (A2)
	(B1) edge [->] node [] {} (C1)
	(B1) edge [->] node [] {} (C2)
	
	(B2) edge [->] node [] {} (A1)
	(B2) edge [->] node [] {} (A2)
	(B2) edge [->] node [] {} (C1)
	(B2) edge [->] node [] {} (C2)
	
	(C1) edge [->] node [] {} (A1)
	(C1) edge [->] node [] {} (A2)
	(C1) edge [->] node [] {} (B1)
	(C1) edge [->] node [] {} (B2)
	
	(C2) edge [->] node [] {} (A1)
	(C2) edge [->] node [] {} (A2)
	(C2) edge [->] node [] {} (B1)
	(C2) edge [->] node [] {} (B2)
	
	% Self loops
	(A1) edge [->, loop below] node [] {} (A1)
	(A2) edge [->, loop below] node [] {} (A2)
	(B1) edge [->, loop left] node [] {} (B1)
	(B2) edge [->, loop left] node [] {} (B2)
	(C1) edge [->, loop right] node [] {} (C1)
	(C2) edge [->, loop right] node [] {} (C2);
	\end{tikzpicture}	
	\caption{The new BN \(G_B\) constructed from the SRG \(G\) in Figure \ref{figure:ExampleGraph3Nodes}.}
	\label{figure:BNTransformExampleGraph}
\end{figure}